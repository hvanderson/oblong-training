DIRS = \
  01-pools\
  02-handipoints\
  01-pools\
  02-handipoints\
  03a-texquad\
  03b-vidquad\
  03c-glypho\
  04-softs\
  05a-wranglers\
  05b-wrangler-zoo\
  06-native-gl\
  07-respiration\
  08a-blurts\
  08b-pointing\
  08c-multi-machine\
  09-viddles\
	tools\
#

all:
	set -e; for d in $(DIRS); do $(MAKE) -C $$d ; done

clean:
	set -e; for d in $(DIRS); do $(MAKE) -C $$d clean ; done
