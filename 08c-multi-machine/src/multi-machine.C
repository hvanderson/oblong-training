
/* (c)  oblong industries */


#include <libLoam/c++/ArgParse.h>
#include <libBasement/LogLogger.h>
#include <libNoodoo/ObStyle.h>
#include <libNoodoo/VFBunch.h>
#include <libNoodoo/VisiDrome.h>

//  wand & glove interaction
#include <libTwillig/HandiPoint.h>

#include "Backing.h"

using namespace oblong::loam;
using namespace oblong::basement;
using namespace oblong::noodoo;
using namespace oblong::afferent;
using namespace oblong::twillig;
using namespace oblong::sample;

static void AppendToAllVisiFelds (ShowyThing *thing);


ObRetort Setup (VFBunch* bunch, Atmosphere* atm)
{ //  get first visifeld
  VisiFeld *vf = VisiFeld::NthOfAllVisiFelds (0);
  Backing::SetupPointing (vf);

  const int N = VisiFeld::NumAllVisiFelds ();
  for (int i = 0  ;  i < N  ;  ++i)
    { VisiFeld *_vf = VisiFeld::NthOfAllVisiFelds (i);
      _vf -> SetBackgroundColor (ObColor (0.22, 0.22, 0.22, 1.0));
    }

  //  create backing
  Backing *backing = new Backing ();
  backing -> OrientLikeFeldHard (vf);
  AppendToAllVisiFelds (backing);


  //  create handipoints
  for (int i = 0  ;  i < 8 ;  i++)
    { HandiPoint *hp = new HandiPoint ();
      hp -> SetShouldShowProvenance (true);
      hp -> SetShouldShowOffFeldMarker (true);

      Backing::ListenToPointing (hp);
      AppendToAllVisiFelds (hp);
    }

  return OB_OK;
}

static void AppendToAllVisiFelds (ShowyThing *thing)
{ const int N = VisiFeld::NumAllVisiFelds ();
  for (int i = 0  ;  i < N  ;  ++i)
    { VisiFeld *vf = VisiFeld::NthOfAllVisiFelds (i);
      vf -> AppendChild (thing);
    }
}


int main (int ac, char **av)
{ // parse command line arguments
  ArgParse ap (ac, av, "[protein files...]");
  ap . ArgString ("message-pool", "\aname of message pool",
                  &Backing::pool_name);

  ap . EasyFinish (0, ArgParse::UNLIMITED);

  OB_LOG_INFO ("using message pool: %s", Backing::pool_name. utf8 ());

  const Str dromename = "multi-machine";
  VisiDrome *instance = new VisiDrome (dromename, ap . Leftovers ());
  instance -> SetAlwaysReadyToRespire (true);
  instance -> SetLogger (new LogLogger);
  instance -> FindVFBunch () -> AppendPostFeldInfoHook (Setup, "post_hook");
  instance -> Respire ();
  instance -> Delete ();

  return (EXIT_SUCCESS);
}

