#!/bin/bash

SERVER=$1

echo "server = $SERVER"

#
ssh espn@$SERVER "\
     	cd ~/demo/multi-machine 
	pkill qm-provisioner 
	pkill qm-viddle
	nohup /usr/bin/qm-provisioner qm bm.conf
"
# !TODO: why nohup needed
