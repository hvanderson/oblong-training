#ifndef MY_TEX_QUAD_H
#define MY_TEX_QUAD_H

#include <libNoodoo/TexQuad.h>
#include <libImpetus/OEPointing.h>

namespace oblong  {

namespace sample {

class MyTexQuad  :  public  noodoo::TexQuad,
                    public impetus::OEPointing::Evts
{ PATELLA_SUBCLASS (MyTexQuad, noodoo::TexQuad);
OB_PRIVATE:
  loam::Str owner;
  bool      selected;

  loam::Vect base_loc;
  loam::Vect select_origin;

  loam::Vect reset_loc;
  loam::Vect reset_norm;
  loam::Vect reset_over;

  bool ShouldReset (impetus::OEPointingEvent *ev) const;
public:
  MyTexQuad (basement::ImageClot *ic);

  void SetResetParams (const loam::Vect &loc,
                       const loam::Vect &norm, const loam::Vect &over);

  void Reset ();

  loam::ObRetort MetabolizeTexReset (const plasma::Protein &prt,
                                     basement::Atmosphere *atm);

  loam::ObRetort MetabolizeTexSelected (const plasma::Protein &prt,
                                        basement::Atmosphere *atm);

  //  *NEW*
  virtual loam::ObRetort OEPointingHarden (impetus::OEPointingHardenEvent *e,
            basement::Atmosphere *atm);
  virtual loam::ObRetort OEPointingSoften (impetus::OEPointingSoftenEvent *e,
            basement::Atmosphere *atm);
  virtual loam::ObRetort OEPointingMove   (impetus::OEPointingMoveEvent   *e,
            basement::Atmosphere *atm);

};


}  } //  namespaces

#endif
