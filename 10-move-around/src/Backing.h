/* (c)  Oblong Industries */


#ifndef BACKING_H
#define BACKING_H

#include <libNoodoo/TexQuad.h>
#include <libNoodoo/VidQuad.h>
#include <libNoodoo/FlatThing.h>
#include <libAfferent/RoddedGlimpser.h>

#include <projects/quartermaster/ViddleSet.h>

namespace oblong {

namespace sample {


class Backing  :  public noodoo::FlatThing
{ PATELLA_SUBCLASS (Backing, noodoo::FlatThing);
OB_PROTECTED:
  //  *NEW*
  loam::ObMap <loam::Str, noodoo::TexQuad*,
               loam::NoMemMgmt, loam::WeakRef> images_by_uid;

  // available viddles
  loam::ObMap <loam::Str, noodoo::VidQuad*,
               loam::NoMemMgmt, loam::WeakRef> vids_by_name;

  //  instanciated vids
  loam::ObMap <loam::Str, noodoo::VidQuad*,
               loam::NoMemMgmt, loam::WeakRef> vids_by_uid;
public:
  Backing ();

  loam::ObRetort MetabolizeNewImage (const plasma::Protein &prt,
                                     basement::Atmosphere *atm);

  loam::ObRetort MetabolizeMoveImage (const plasma::Protein &prt,
                                      basement::Atmosphere *atm);


  loam::ObRetort MetabolizeNewVideo (const plasma::Protein &prt,
                                     basement::Atmosphere *atm);
  loam::ObRetort MetabolizeNewVideoWall (const plasma::Protein &prt,
                                     basement::Atmosphere *atm);

  loam::ObRetort MetabolizeMoveVideo (const plasma::Protein &prt,
                                      basement::Atmosphere *atm);



  void AddViddle (quartermaster::Viddle *v);
  void RemoveViddle (quartermaster::Viddle *v);

  static loam::ObRetort ViddleLive (quartermaster::ViddleSet *vs,
                                    quartermaster::Viddle *v,
                                    basement::Atmosphere *atm,
                                    Backing *);

  static loam::ObRetort ViddleDied (quartermaster::ViddleSet *vs,
                                    quartermaster::Viddle *v,
                                    basement::Atmosphere *atm,
                                    Backing*);


  virtual loam::ObRetort PreDraw (noodoo::VisiFeld* vf, 
				  basement::Atmosphere *atm);
	

  //  some static helper stuff
  static loam::Str pool_name;
  static afferent::RoddedGlimpser *wanding;
  static afferent::RoddedGlimpser *gripe_pointing;
  static void SetupPointing (basement::SpaceFeld *context);
  static void ListenToPointing (basement::KneeObject *obj);
};

}  } // namespaces

#endif
