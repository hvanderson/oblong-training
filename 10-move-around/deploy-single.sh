#!/bin/bash

SERVER=$1


ssh espn@$SERVER "\
  mkdir -p ~/demo/multi-machine
"
rsync -avz multi-machine *.png *.protein *.conf espn@$SERVER:~/demo/multi-machine

