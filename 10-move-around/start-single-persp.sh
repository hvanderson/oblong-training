#!/bin/bash

SERVER=$1

echo "server = $SERVER"

#pkill qm-provisioner 
#pkill qm-viddle
# (/usr/bin/qm-provisioner qm bm.conf) &

ssh espn@$SERVER "\
      export DISPLAY=:0.0 
      pkill multi-machine 
      cd ~/demo/multi-machine 
      ./multi-machine screens.protein --perspective --message-pool tcp://et-ubuntu01/demo-app\
"

