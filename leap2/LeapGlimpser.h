
/* (c)  oblong industries */

#include <libBasement/ob-logging-macros.h>
#include <libAfferent/Glimpser.h>
#include <libImpetus/OEBlurt.h>
#include "OELeap.h"

using namespace oblong::loam;
using namespace oblong::basement;
using namespace oblong::afferent;


struct LMFrame {
  int64 id, timestamp;
  ObMap <int64, OELeapPointingEvent *> pointers;
  ObMap <int64, OELeapHandEvent *> hands;

  LMFrame () : id (0), timestamp (0) {}

  bool Read (Slaw const &s)
  { bool ret = false;
    if (s . MapFind ("id") . Into (id) &&
        s . MapFind ("ts") . Into (timestamp))
      { Slaw ps = s . MapFind ("pntrs");
        for (int64 i = 0; i < ps . Count (); ++i)
          { OELeapPointingMoveEvent *pe =
              new OELeapPointingMoveEvent (Protein (Slaw::List (), ps . Nth (i)));
            pointers . Put (pe -> id, pe);
          }

        Slaw hs = s . MapFind ("hands");
        for (int64 i = 0; i < hs . Count (); ++i)
          { OELeapHandMoveEvent *he =
              new OELeapHandMoveEvent (Protein (Slaw::List (), hs . Nth (i)));
            hands . Put (he -> id, he);
          }

        ret = true;
      }
    return ret;
  }
};

/**
    Defines an interface for classes that would like to
    listen for special leap-generated events such as tap,
    circle, etc.

    Also provides the handler function which knows how to
    deal with input from the 'leap' pool, process the protein,
    and call the right event handlers.

    To use this, inherit from it.  Then connect the
    MetabolizeLeapProtein() handler to the 'leap' pool with
    the helper function found below:

        ListenForLeapEvents (this, "leap");

 */
class LeapGlimpser : public Glimpser
{ PATELLA_SUBCLASS (LeapGlimpser, Glimpser);
public:
  LeapGlimpser (const Str &leap_pool = "leap") : Glimpser ()
  { UrDrome *drome = UrDrome::OutermostDrome ();
    if (! drome)
      { CRITICAL ("LeapGlimpser constructor can't find outermost drome...");
        return;
      }

    drome -> PoolParticipate (leap_pool, this);

    AppendMetabolizer (Slaw::List ("leap"),
                       &LeapGlimpser::MetabolizeLeapProtein,
                       "leap-ob-event");
  }

  virtual ObRetort MetabolizeLeapProtein (Protein const &p, Atmosphere *atm)
  { Slaw frm = p . Ingests () . MapFind ("frame");
    LMFrame frame;
    if (frame . Read (frm))
      { for (int i = 0; i < frame . hands . Count (); i++)
          PortageEvent (frame . hands . NthVal (i), atm);//LeapHand (frame . hands . NthVal (i), frame);
        for (int i = 0; i < frame . pointers . Count (); i++)
          PortageEvent (frame . pointers . NthVal (i), atm);//LeapPointer (frame . pointers . NthVal (i), frame);

        Slaw gsts = frm . MapFind ("gests");
        const int64 N = gsts . Count ();
        for (int64 i = 0; i < N; ++i)
          { Slaw g = gsts . Nth (i);
            Str type;
            g . MapFind ("type") . Into (type);
            Protein p2 = Protein (Slaw::List (), g);
            if ("circle" == type)
              { ObRef <OELeapCircleAppearEvent *> e =
                  new OELeapCircleAppearEvent (p2);
                //(~e) -> SetLeapFrame (frame);
                PortageEvent (~e, atm);
              }
            else if ("swipe" == type)
              { ObRef <OELeapSwipeAppearEvent *> e =
                  new OELeapSwipeAppearEvent (p2);
                //(~e) -> SetLeapFrame (frame);
                PortageEvent (~e, atm);
              }
            else if ("s-tap" == type)
              { ObRef <OELeapScreenTapAppearEvent *> e =
                  new OELeapScreenTapAppearEvent (p2);
                //(~e) -> SetLeapFrame (frame);
                PortageEvent (~e, atm);
              }
            else if ("k-tap" == type)
              { ObRef <OELeapKeyTapAppearEvent *> e =
                  new OELeapKeyTapAppearEvent (p2);
                //(~e) -> SetLeapFrame (frame);
                PortageEvent (~e, atm);
              }
          }
      }
    return OB_OK;
  }
};

class LeapToOEPntgGlimpser  : public Glimpser,
                              public OELeap::Evts
{ PATELLA_SUBCLASS (LeapToOEPntgGlimpser, Glimpser);
private:
  ObMap <Str, FatherTime> pointers;

 public:
  LeapToOEPntgGlimpser (LeapGlimpser *lg = NULL);
  ~LeapToOEPntgGlimpser () {}

  /// Remove any idle pointers from the scene
  void CleanupDeadEvents ();

  ObRetort OELeapPointingMove (OELeapPointingMoveEvent *e, Atmosphere *atm);
};

class LeapToOEBlurtGlimpser  :  public Glimpser,
                                public OELeap::Evts
{ PATELLA_SUBCLASS (LeapToOEBlurtGlimpser, Glimpser);
private:
  ObMap <int64, FatherTime> gestures;
  ObMap <int64, OEBlurtEvent *> prev_gest;

 public:
  LeapToOEBlurtGlimpser (LeapGlimpser *lg = NULL);
  ~LeapToOEBlurtGlimpser () {}

  /// Remove any idle pointers from the scene
  void CleanupDeadEvents ();

  ObRetort OELeapCircleAppear (OELeapCircleAppearEvent *e, Atmosphere *atm);
  ObRetort OELeapSwipeAppear (OELeapSwipeAppearEvent *e, Atmosphere *atm);
  ObRetort OELeapScreenTapAppear (OELeapScreenTapAppearEvent *e,
                                  Atmosphere *atm);
  ObRetort OELeapKeyTapAppear (OELeapKeyTapAppearEvent *e, Atmosphere *atm);
};


/**
* Convenience Methods
**/

/*bool PointerFromGesture (LMGesture const&s, LMFrame const &f, LMPointer &p)
{ if (f . pointers . KeyIsPresent (s . pointer_id))
    { p = f . pointers . Find (s . pointer_id);
      return true;
    }
  return false;
  }*/
