
/* (c)  oblong industries */

#ifndef OE_LEAPING_LORDS
#define OE_LEAPING_LORDS


#include <libImpetus/ObEvent.h>

namespace oblong {  namespace impetus {
using namespace oblong::basement;
using namespace oblong::plasma;
using namespace oblong::loam;


class OB_IMPETUS_API OELeapHandEvent  :  public ObEvent
{ PATELLA_SUBCLASS (OELeapHandEvent, ObEvent);
  OE_MISCY_MAKER (OELeapHand, OE, "leap-hand");

 public:
  int64 id;
  Vect direction;
  Vect palm_normal;
  Vect palm_position;
  Vect palm_velocity;
  Vect sphere_center;
  Vect phys_orig;
  Vect phys_through;
  float64 sphere_radius;

  OELeapHandEvent () : ObEvent (), id (0), sphere_radius (0) {}

  int64 ID () { return id; }
  Vect PhysOrigin () { return phys_orig; }
  Vect PhysThrough () { return phys_through; }

  virtual ObRetort AnalyzeInternalProtein ()
  { Slaw ing = evpro . Ingests ();
    ing . MapFind ("id") . Into (id);
    ing . MapFind ("dir") . Into (direction);
    ing . MapFind ("plmnrm") . Into (palm_normal);
    ing . MapFind ("plmpos") . Into (palm_position);
    ing . MapFind ("plmvel") . Into (palm_velocity);
    ing . MapFind ("center") . Into (sphere_center);
    ing . MapFind ("radius") . Into (sphere_radius);
    ing . MapFind ("orig") . Into (phys_orig);
    ing . MapFind ("thru") . Into (phys_through);
    SetProvenance (Str::Format ("leap-%" OB_FMT_64 "d", id));
    return OB_OK;
  }
};

class OELeapHandAppearEvent  :  public OELeapHandEvent
{ PATELLA_SUBCLASS (OELeapHandAppearEvent, OELeapHandEvent);
  OE_MISCY_MAKER (OELeapHandAppear, OELeapHand, "appear");
  OELeapHandAppearEvent ()  :  OELeapHandEvent ()     { }
};

class OELeapHandMoveEvent  :  public OELeapHandEvent
{ PATELLA_SUBCLASS (OELeapHandMoveEvent, OELeapHandEvent);
  OE_MISCY_MAKER (OELeapHandMove, OELeapHand, "move");
  OELeapHandMoveEvent ()  :  OELeapHandEvent ()     { }
};

class OELeapHandVanishEvent  :  public OELeapHandEvent
{ PATELLA_SUBCLASS (OELeapHandVanishEvent, OELeapHandEvent);
  OE_MISCY_MAKER (OELeapHandVanish, OELeapHand, "vanish");
  OELeapHandVanishEvent ()  :  OELeapHandEvent ()     { }
};

class OB_IMPETUS_API OELeapPointingEvent  :  public ObEvent
{ PATELLA_SUBCLASS (OELeapPointingEvent, ObEvent);
  OE_MISCY_MAKER (OELeapPointing, OE,"leap-pointing");

 public:
   int64 id;
   Vect direction;
   int64 hand_id;
   bool is_finger;
   bool is_tool;
   float64 length;
   Vect tip_position;
   Vect tip_velocity;
   Vect phys_orig;
   Vect phys_through;
   float64 width;

   OELeapPointingEvent () : ObEvent (), id (0), hand_id (0), is_finger (false),
                            is_tool (false), length (0), width (0)
   { }

   int64 ID () { return id; }
   Vect PhysOrigin () { return phys_orig; }
   Vect PhysThrough () { return phys_through; }

   virtual ObRetort AnalyzeInternalProtein ()
   { Slaw ing = evpro . Ingests ();
     ing . MapFind ("id") . Into (id);
     ing . MapFind ("dir") . Into (direction);
     ing . MapFind ("hand") . Into (hand_id);
     ing . MapFind ("isfngr") . Into (is_finger);
     ing . MapFind ("istool") . Into (is_tool);
     ing . MapFind ("length") . Into (length);
     ing . MapFind ("t-pos") . Into (tip_position);
     ing . MapFind ("t-vel") . Into (tip_velocity);
     ing . MapFind ("width") . Into (width);
     ing . MapFind ("orig") . Into (phys_orig);
     ing . MapFind ("thru") . Into (phys_through);
     SetProvenance (Str::Format ("leap-%" OB_FMT_64 "d", id));
     return OB_OK;
   }  
};

class OELeapPointingAppearEvent  :  public OELeapPointingEvent
{ PATELLA_SUBCLASS (OELeapPointingAppearEvent, OELeapPointingEvent);
  OE_MISCY_MAKER (OELeapPointingAppear, OELeapPointing, "appear");
  OELeapPointingAppearEvent ()  :  OELeapPointingEvent ()     { }
};

class OELeapPointingMoveEvent  :  public OELeapPointingEvent
{ PATELLA_SUBCLASS (OELeapPointingMoveEvent, OELeapPointingEvent);
  OE_MISCY_MAKER (OELeapPointingMove, OELeapPointing, "move");
  OELeapPointingMoveEvent ()  :  OELeapPointingEvent ()     { }
};

class OELeapPointingVanishEvent  :  public OELeapPointingEvent
{ PATELLA_SUBCLASS (OELeapPointingVanishEvent, OELeapPointingEvent);
  OE_MISCY_MAKER (OELeapPointingVanish, OELeapPointing, "vanish");
  OELeapPointingVanishEvent ()  :  OELeapPointingEvent ()     { }
};


class OB_IMPETUS_API OELeapGestureEvent  :  public ObEvent
{ PATELLA_SUBCLASS (OELeapGestureEvent, ObEvent);
  OE_MISCY_MAKER (OELeapGesture, OE, "leap-gestures");

 public:
  int64 id;
  int64 duration_microseconds;
  float64 duration_seconds;
  ObTrove <int64> hand_ids;
  ObTrove <int64> pointer_ids;
  Str state;
  Str type;
  int64 pointer_id;
//  ObRef <LMFrame *> frame;

  OELeapGestureEvent ()  :  ObEvent (), id (0), duration_microseconds (0),
                            duration_seconds (0)//, frame (NULL)
  { }
  ~OELeapGestureEvent () { }

  int64 ID () { return id; }
  int64 Duration () { return duration_seconds; }
  Str State ()      { return state; }
  Str Type ()       { return type; }

  void ReadHands (Slaw const &ps)
  { const int64 N = ps . Count ();
    int64 hid;
    for (int64 i = 0; i < N; ++i)
      if (ps . Nth (i) . Into (hid))
        hand_ids . Append (hid);
  }

  void ReadPointers (Slaw const &ps)
  { const int64 N = ps . Count ();
    int64 pid;
    for (int64 i = 0; i < N; ++i)
      if (ps . Nth (i) . Into (pid))
        pointer_ids . Append (pid);
  }

  virtual ObRetort AnalyzeInternalProtein ()
  { Slaw ing = evpro . Ingests ();
    ing . MapFind ("id") . Into (id);
    ing . MapFind ("dur") . Into (duration_microseconds);
    ing . MapFind ("dursec") . Into (duration_seconds);
    ReadHands (ing . MapFind ("hands"));
    ReadPointers (ing . MapFind ("pntrs"));
    ing . MapFind ("state") . Into (state);
    ing . MapFind ("type") . Into (type);
    ing . MapFind ("point") . Into (pointer_id);
    SetProvenance (Str::Format ("leap-%" OB_FMT_64 "d", id));
    return OB_OK;
  }

 //  LMFrame *Frame ()                 { return ~frame; }

  // void SetLeapFrame (LMFrame *lf)
  //   { frame = lf; }
};

class OB_IMPETUS_API OELeapCircleEvent  :  public OELeapGestureEvent
{ PATELLA_SUBCLASS (OELeapCircleEvent, OELeapGestureEvent);
  OE_MISCY_MAKER (OELeapCircle, OELeapGesture, "leap-circle");

 public:
  Vect center;
  Vect normal;
  float64 progress;
  float64 radius;

  OELeapCircleEvent ()  :  OELeapGestureEvent (), progress (1.0), radius (0)
  { SetWordstamp ("leap-circle"); }

  Vect Center () { return center; }
  Vect Normal () { return normal; }
  float64 Progress () { return progress; }
  float64 Radius () { return radius; }

  virtual ObRetort AnalyzeInternalProtein ()
  { super::AnalyzeInternalProtein ();
    Slaw ing = evpro . Ingests ();
    ing . MapFind ("cent") . Into (center);
    ing . MapFind ("norm") . Into (normal);
    ing . MapFind ("prog") . Into (progress);
    ing . MapFind ("radius") . Into (radius);
    SetWordstamp ("leap-circle");
    return OB_OK;
  }
};

class OELeapCircleAppearEvent  :  public OELeapCircleEvent
{ PATELLA_SUBCLASS (OELeapCircleAppearEvent, OELeapCircleEvent);
  OE_MISCY_MAKER (OELeapCircleAppear, OELeapCircle, "appear");
  OELeapCircleAppearEvent ()  :  OELeapCircleEvent ()     { }
};

class OB_IMPETUS_API OELeapSwipeEvent  :  public OELeapGestureEvent
{ PATELLA_SUBCLASS (OELeapSwipeEvent, OELeapGestureEvent);
  OE_MISCY_MAKER (OELeapSwipe, OELeapGesture,"leap-swipe");

 public:
  Vect direction;
  Vect position;
  float64 speed;
  Vect starting_position;

  OELeapSwipeEvent ()  :  OELeapGestureEvent (), speed (0)
  { SetWordstamp ("leap-swipe"); }

  Vect Direction () { return direction; }
  Vect Position ()  { return position; }
  float64 Speed ()  { return speed; }
  Vect EstabLoc ()  { return starting_position; }

  virtual ObRetort AnalyzeInternalProtein ()
  { super::AnalyzeInternalProtein ();
    Slaw ing = evpro . Ingests ();
    ing . MapFind ("dir") . Into (direction);
    ing . MapFind ("pos") . Into (position);
    ing . MapFind ("speed") . Into (speed);
    ing . MapFind ("start") . Into (starting_position);
    SetWordstamp ("leap-swipe");
    return OB_OK;
  }
};

class OELeapSwipeAppearEvent  :  public OELeapSwipeEvent
{ PATELLA_SUBCLASS (OELeapSwipeAppearEvent, OELeapSwipeEvent);
  OE_MISCY_MAKER (OELeapSwipeAppear, OELeapSwipe, "appear");
  OELeapSwipeAppearEvent ()  :  OELeapSwipeEvent ()     { }
};

class OB_IMPETUS_API OELeapScreenTapEvent  :  public OELeapGestureEvent
{ PATELLA_SUBCLASS (OELeapScreenTapEvent, OELeapGestureEvent);
  OE_MISCY_MAKER (OELeapScreenTap, OELeapGesture, "leap-screen-tap");

 public:
  Vect direction;
  Vect position;
  float64 progress; // always 1.0

  OELeapScreenTapEvent ()  :  OELeapGestureEvent (), progress (1.0)
  { SetWordstamp ("leap-screen-tap"); }

  Vect Direction () { return direction; }
  Vect Position ()  { return position; }

  virtual ObRetort AnalyzeInternalProtein ()
  { super::AnalyzeInternalProtein ();
    Slaw ing = evpro . Ingests ();
    ing . MapFind ("dir") . Into (direction);
    ing . MapFind ("pos") . Into (position);
    ing . MapFind ("prog") . Into (progress);
    SetWordstamp ("leap-screen-tap");
    return OB_OK;
  }
};

class OELeapScreenTapAppearEvent  :  public OELeapScreenTapEvent
{ PATELLA_SUBCLASS (OELeapScreenTapAppearEvent, OELeapScreenTapEvent);
  OE_MISCY_MAKER (OELeapScreenTapAppear, OELeapScreenTap, "appear");
  OELeapScreenTapAppearEvent ()  :  OELeapScreenTapEvent ()     { }
};

class OB_IMPETUS_API OELeapKeyTapEvent  :  public OELeapGestureEvent
{ PATELLA_SUBCLASS (OELeapKeyTapEvent, OELeapGestureEvent);
  OE_MISCY_MAKER (OELeapKeyTap, OELeapGesture,"leap-key-tap");

 public:
  Vect direction;
  Vect position;
  float64 progress; // always 1.0

  OELeapKeyTapEvent ()  :  OELeapGestureEvent (), progress (1.0)
  { SetWordstamp ("leap-key-tap"); }

  Vect Direction () { return direction; }
  Vect Position ()  { return position; }

  virtual ObRetort AnalyzeInternalProtein ()
  { super::AnalyzeInternalProtein ();
    Slaw ing = evpro . Ingests ();
    ing . MapFind ("dir") . Into (direction);
    ing . MapFind ("pos") . Into (position);
    ing . MapFind ("prog") . Into (progress);
    SetWordstamp ("leap-key-tap");
    return OB_OK;
  }
};

class OELeapKeyTapAppearEvent  :  public OELeapKeyTapEvent
{ PATELLA_SUBCLASS (OELeapKeyTapAppearEvent, OELeapKeyTapEvent);
  OE_MISCY_MAKER (OELeapKeyTapAppear, OELeapKeyTap, "appear");
  OELeapKeyTapAppearEvent ()  :  OELeapKeyTapEvent ()     { }
};


class OB_IMPETUS_API OELeapEventAcceptorGroup
  :  public  OELeapPointingAppearEvent :: OELeapPointingAppearAcceptor,
     public  OELeapPointingVanishEvent :: OELeapPointingVanishAcceptor,
     public    OELeapPointingMoveEvent :: OELeapPointingMoveAcceptor,
     public      OELeapHandAppearEvent :: OELeapHandAppearAcceptor,
     public      OELeapHandVanishEvent :: OELeapHandVanishAcceptor,
     public        OELeapHandMoveEvent :: OELeapHandMoveAcceptor,
     public    OELeapCircleAppearEvent :: OELeapCircleAppearAcceptor,
     public     OELeapSwipeAppearEvent :: OELeapSwipeAppearAcceptor,
     public OELeapScreenTapAppearEvent :: OELeapScreenTapAppearAcceptor,
     public    OELeapKeyTapAppearEvent :: OELeapKeyTapAppearAcceptor
{ };



class OELeap
{ public:
  typedef OELeapEventAcceptorGroup Evts;
};

} }  // beat it, namespace impetus! you too, namespace oblong!


DECLARE_INTERFACE_AS_ANKLE_OBJECT_SPECIALIZED
  (oblong::impetus::OELeap::Evts);



#endif
