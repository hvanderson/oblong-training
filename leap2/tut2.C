/* (c)  oblong industries */

#include <libLoam/c/ob-rand.h>
#include <libLoam/c/ob-vers.h>
#include <libLoam/c++/ArgParse.h>
#include <libBasement/LinearColor.h>
#include <libAfferent/Gestator.h>
#include <libAfferent/DualCase.h>
#include <libAfferent/MouseGlimpser.h>
#include <libAfferent/KeyboardGlimpser.h>
#include <libImpetus/OEComprehensive.h>
#include <libNoodoo/GlyphoString.h>
#include <libNoodoo/ObStyle.h>
#include <libNoodoo/VisiDrome.h>
#include <libTwillig/HandiPoint.h>

#include "LeapGlimpser.h"

using namespace oblong::basement;
using namespace oblong::afferent;
using namespace oblong::impetus;
using namespace oblong::noodoo;
using namespace oblong::twillig;

/** This tutorial shows the basic usage and setup when integration ultrasonic
    or optical wands
*/

ObColor RandColor ()
{ return  ObColor (ob_rand_float64 (0.0, 1.0),
                   ob_rand_float64 (0.0, 1.0),
                   ob_rand_float64 (0.0, 1.0));
}

// a simple gl line class
class Squirly : public ShowyThing
{
 private:
  ObTrove <Vect> verts;
  ObColor c;

 public:

  Squirly () : ShowyThing ()
    { c = RandColor (); }

  void AppendPoint (Vect p)
    { if (p . IsInvalid ())
        return;
      verts . Insert (p, 0);
      if (verts . Count () > 100)
        verts . RemoveNth (100);
    }

  void DrawSelf (VisiFeld *vf, Atmosphere *a)
    { glColor4f (c.r, c.g, c.b, c.a);
      glEnable (GL_LINE_SMOOTH);
      glBegin (GL_LINE_STRIP);
      for (int i = 0; i < verts . Count (); i++)
        { Vect v = verts . Nth (i);
          glVertex3d (v.x, v.y, v.z);
        }
      glEnd ();
      glDisable (GL_LINE_SMOOTH);
    }
};

// DrawParent listens only to traditional pointing and blurt events
class DrawParent : public ShowyThing,
                   public OEComprehensive::Evts
{
private:
  ObMap <Str, Squirly *> squirlies;
  ObMap <Str, FatherTime> pointers;
  ObRef <GlyphoString *> blurty;
  float64 start_font_size;

public:
  DrawParent () : ShowyThing ()
  { SetShouldTravail (true);

    VisiFeld *vf = VisiFeld::NthOfAllVisiFelds (0);
    GlyphoString *gs = new GlyphoString ();
    start_font_size = 0.05 * vf -> Height ();
    gs -> SetFontSize (start_font_size);
    gs -> SetFont (ObStyle::MediumFontPath ());
    gs -> SetTextColor (ObColor (1.0, 1.0));
    gs -> SetLoc (vf -> Loc ());
    blurty = gs;
    AppendChild (~blurty);
  }

  ObRetort Travail (Atmosphere *atm)
  { for (int i = pointers . Count () - 1; i >= 0; i--)
      { FatherTime t = pointers . NthVal (i);
        if (t . CurTime () > 2.0)
          { Squirly *s = squirlies . Find (pointers . NthKey (i));
            squirlies . Remove (pointers . NthKey (i));
            pointers . RemoveNth (i);
            RemoveChild (s);
          }
      }
    return OB_OK;
  }

  ObRetort OEPointingMove (OEPointingMoveEvent *e, Atmosphere *atm)
  { Squirly *s = squirlies . Find (e -> Provenance ());
    if (! s)
      { s = new Squirly ();
        AppendChild (s);
        squirlies . Put (e -> Provenance (), s);
        FatherTime t;
        pointers . Put (e -> Provenance (), t);
      }
    Vect hit;
    if (SpaceFeld::ClosestWhackedFeld (e -> PhysOrigin (), e -> PhysThrough (),
                                       &hit))
      { s -> AppendPoint (hit);
        pointers . ValFromKey (e -> Provenance ()) . ZeroTime ();
      }
    return OB_OK;
  }

  ObRetort OEBlurtAppear (OEBlurtAppearEvent *e, Atmosphere *atm)
  { (~blurty) -> SetString (e -> Utterance ());
    (~blurty) -> SetTextColor (RandColor ());
    (~blurty) -> SetFontSize (start_font_size);
    return OB_OK;
  }

  ObRetort OEBlurtRepeat (OEBlurtRepeatEvent *e, Atmosphere *atm)
  { (~blurty) -> SetString (e -> Utterance ());
    (~blurty) -> SetFontSize (1.1 * (~blurty) -> FontSize ());
    return OB_OK;
  }

};

// The LeapHandler listens to full blown leap events
class LeapHandler : public KneeObject,
                    public OELeap::Evts
{
public:
  LeapHandler (const Str &leap_pool = "leap") : KneeObject ()
  {}

  ObRetort OELeapPointingMove (OELeapPointingMoveEvent *e, Atmosphere *atm)
  { // do stuff
    OB_LOG_INFO ("leap pointing (%s): loc (%s)", e -> Provenance () . utf8 (),
                 e -> PhysOrigin () . AsStr () . utf8 ());
    return OB_OK;
  }

  ObRetort OELeapHandMove (OELeapHandMoveEvent *e, Atmosphere *atm)
  { OB_LOG_INFO ("leap hand (%s): loc (%s)", e -> Provenance () . utf8 (),
                 e -> PhysOrigin () . AsStr () . utf8 ());
    return OB_OK;
  }

  ObRetort OELeapCircleAppear (OELeapCircleAppearEvent *e, Atmosphere *atm)
  { OB_LOG_WARNING ("leap circle (%s):\n"
                      "    center = %s\n"
                      "    radius = %f\n"
                      "    num rotations = %f",
                      e -> Provenance () . utf8 (),
                      e -> Center () . AsStr () . utf8 (),
                      e -> Radius (), e -> Progress ());
    return OB_OK;
  }

  ObRetort OELeapSwipeAppear (OELeapSwipeAppearEvent *e, Atmosphere *atm)
  { OB_LOG_WARNING ("leap swipe (%s):\n"
                    "    dir = %s\n"
                    "    speed = %f\n",
                    e -> Provenance () . utf8 (),
                    e -> Direction () . AsStr () . utf8 (),
                    e -> Speed ());
    return OB_OK;
  }

  ObRetort OELeapScreenTapAppear (OELeapScreenTapAppearEvent *e, Atmosphere *atm)
  { OB_LOG_WARNING ("screen tap (%s):\n"
                    "    dir = %s\n",
                    e -> Provenance () . utf8 (),
                    e -> Direction () . AsStr () . utf8 ());
    return OB_OK;
  }

  ObRetort OELeapKeyTapAppear (OELeapKeyTapAppearEvent *e, Atmosphere *atm)
  { OB_LOG_WARNING ("key tap (%s):\n"
                    "    dir = %s\n",
                    e -> Provenance () . utf8 (),
                    e -> Direction () . AsStr () . utf8 ());
    return OB_OK;
  }
};

ObTrove <HandiPoint *> pointers;
DrawParent *dp;

ObRetort Setup (VisiDrome *drome, VisiFeld *vf, Atmosphere *atm)
{ if (pointers . Count () > 0)
    { ObCrawl <HandiPoint *> cr = pointers . Crawl ();
      while (! cr . isempty ())
        vf -> AppendChild (cr . popfore ());
      vf -> AppendChild (dp);
      return OB_OK;
    }

  Str leap_pool = "leap";
  Str lpoo (getenv ("LEAP_POOL"));
  if (! lpoo . IsNull () && ! lpoo . IsEmpty ())
    leap_pool = lpoo;
  OB_LOG_INFO ("using leap pool %s\n", leap_pool . utf8 ());

  Gestator *g = new Gestator;
  drome -> AppendChild (g);

  /// enable leap.
  LeapGlimpser *lglim = new LeapGlimpser (leap_pool);
  g -> AppendGlimpser (lglim);
  /// enable pointing.
  LeapToOEPntgGlimpser *pglim = new LeapToOEPntgGlimpser (lglim);
  g -> AppendGlimpser (pglim);
  LeapToOEBlurtGlimpser *bglim = new LeapToOEBlurtGlimpser (lglim);
  g -> AppendGlimpser (bglim);

  // enable mouse and keyboard events
  MouseGlimpser *mgl = new MouseGlimpser;
  g -> AppendGlimpser (mgl);
  KeyboardGlimpser *kgl = new KeyboardGlimpser;
  g -> AppendGlimpser (kgl);

  // set up the LeapHandler and register it for leap events
  LeapHandler * lh = new LeapHandler;
  lglim -> AppendEventTarget (lh);

  // set up the HandiPoints and register them for pointing events
  for (int i = 0; i < 10; i++)
    { HandiPoint *hp = new HandiPoint ();
      hp -> SetIsExclusive (false);
      vf -> AppendChild (hp);
      pointers . Append (hp);
      pglim -> AppendEventTarget (hp);
      mgl -> AppendEventTarget (hp);
    }

  // set up the DrawParent and register it for pointing & blurt events
  dp = new DrawParent ();
  vf -> AppendChild (dp);
  pglim -> AppendEventTarget (dp);
  bglim -> AppendEventTarget (dp);
  mgl -> AppendEventTarget (dp);
  kgl -> AppendEventTarget (dp);

  return OB_OK;
}

int main (int argc, char **argv)
{ printf ("\n");
  printf ("Leap Tutorial - g-speak events");
  ob_banner (stdout); // gspeak info
  printf ("\n");

  bool bHelp = false;

  ArgParse ap;
  ap . ArgFlag ("h", "\adisplay usage information", &bHelp);
  // Parse the input arguments
  if (! ap . Parse (argc, argv))
    { fprintf (stderr, "%s\n", ap . ErrorMessage () . utf8 ());
      return EXIT_FAILURE;
    }

  // Print help message if requested and quit
  if (bHelp)
    { printf ("Usage: tut1 [options]\n");
      printf ("\n");
      printf ("Options:\n");
      printf ("%s\n", ap . UsageMessage () . utf8 ());
      return EXIT_FAILURE;
    }

  VisiDrome *drome = new VisiDrome ();
  drome -> AppendFeldSetupHook (Setup, "setup");
  drome -> Respire ();
  drome -> Delete ();
}
