Setup
Install Leap SDK
  1. Download from: https://developer.leapmotion.com/
  2. Extract download and cd into download
  3. Install leap software:
     $ sudo dpkg -i Leap-0.8.0-x64.deb

Get Splash
  1. Clone/pull latest from: https://github.com/Oblong/splash.git
  2. build splash
     $ cd splash
     $ LEAPSDK_HOME=/path/to/LeapSDK make

=================
Run Leap Pipeline

1. Run Leap (sudo is important)
   $ sudo leapd
2. Run splash
   $ LD_LIBRARY_PATH=/path/to/LeapSDK/lib/x64 ./splash

Run tutorial
  $ cd tut[x]
  $ make
  $ ./tut[x]

Make various gestures and point with your fingers to see the haps.
