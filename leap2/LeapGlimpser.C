
#include "LeapGlimpser.h"

#include <libBasement/TimedTrigger.h>
#include <libImpetus/OEPointing.h>

using namespace oblong::impetus;

struct CleanupTrigger : public TimedTrigger
{ ObRef <LeapToOEPntgGlimpser *> pglim;
  ObRef <LeapToOEBlurtGlimpser *> bglim;
  CleanupTrigger (LeapToOEPntgGlimpser *g, float64 freq) 
    : TimedTrigger (freq), pglim (g), bglim (NULL)
  { }
  CleanupTrigger (LeapToOEBlurtGlimpser *g, float64 freq) 
    : TimedTrigger (freq), pglim (NULL), bglim (g)
  { }

  ObRetort Fire (KneeObject *slf, Atmosphere *atm)
  { if (~pglim)
      (~pglim) -> CleanupDeadEvents ();
    else if (~bglim)
      (~bglim) -> CleanupDeadEvents ();
    return OB_OK;
  }
};


LeapToOEPntgGlimpser::LeapToOEPntgGlimpser (LeapGlimpser *lg)
                                            : Glimpser ()
{ if (lg)
    lg -> AppendEventTarget (this);

  UrDrome *drome = UrDrome::OutermostDrome ();
  if (! drome)
    { CRITICAL ("LeapGlimpser constructor can't find outermost drome...");
      return;
    }

  drome -> AppendTrigger (new CleanupTrigger (this, 1.0));
}

/// Remove any idle pointers from the scene
void LeapToOEPntgGlimpser::CleanupDeadEvents ()
{ for (int i = pointers . Count () - 1; i >= 0; i--)
    { FatherTime t = pointers . NthVal (i);
      if (t . CurTime () > 2.0)
        { ObRef <OEPointingEvent *>pe = new OEPointingLingerEvent ();
          (~pe) -> SetProvenance (pointers . NthKey (i));
          PortageEvent (~pe, NULL);
          pointers . RemoveNth (i);
        }
      else if (t . CurTime () > 1.0)
        { ObRef <OEPointingEvent *>pe = new OEPointingVanishEvent ();
          (~pe) -> SetProvenance (pointers . NthKey (i));
          PortageEvent (~pe, NULL);
        }
    }
}

ObRetort LeapToOEPntgGlimpser::OELeapPointingMove (OELeapPointingMoveEvent *e,
                                                     Atmosphere *atm)
{ Str prov = e -> Provenance ();
  ObRef <OEPointingMoveEvent *> pe = new OEPointingMoveEvent ();
  (~pe) -> SetProvenance (prov);
  (~pe) -> SetPhysOrigAndThrough (e -> PhysOrigin (), e -> PhysThrough ());
  (~pe) -> SetPrevOrigAndThrough (e -> PhysOrigin (), e -> PhysThrough ());
  (~pe) -> SetWordstamp ("leap-pointer");
  if (pointers . KeyIsPresent (prov))
    pointers . Find (prov) . ZeroTime ();
  else
    { FatherTime t;
      pointers . Put (prov, t);
      ObRef <OEPointingAppearEvent *> pea = new OEPointingAppearEvent ();
      (~pea) -> SetProvenance (prov);
      (~pe) -> SetPhysOrigAndThrough (e -> PhysOrigin (), e -> PhysThrough ());
      (~pe) -> SetPrevOrigAndThrough (e -> PhysOrigin (), e -> PhysThrough ());
      (~pea) -> SetWordstamp ("leap-pointer");
      PortageEvent (~pea, atm);
    }
  PortageEvent (~pe, atm);
  return OB_OK;
}

LeapToOEBlurtGlimpser::LeapToOEBlurtGlimpser (LeapGlimpser *lg)
                                             : Glimpser ()
{ if (lg)
    lg -> AppendEventTarget (this);

  UrDrome *drome = UrDrome::OutermostDrome ();
  if (! drome)
    { CRITICAL ("LeapGlimpser constructor can't find outermost drome...");
      return;
    }

  drome -> AppendTrigger (new CleanupTrigger (this, 1.0));
}

/// Remove any idle pointers from the scene
void LeapToOEBlurtGlimpser::CleanupDeadEvents ()
{ for (int i = gestures . Count () - 1; i >= 0; i--)
    { FatherTime t = gestures . NthVal (i);
      if (t . CurTime () > 1.0)
        gestures . RemoveNth (i);
    }
}

ObRetort LeapToOEBlurtGlimpser::OELeapCircleAppear (OELeapCircleAppearEvent *e,
                                                    Atmosphere *atm)
{ int64 id = e -> id;
  ObRef <OEBlurtEvent *> pe = NULL;
  if (! gestures . KeyIsPresent (id))
    { pe = new OEBlurtAppearEvent ();
      FatherTime t;
      gestures . Put (id, t);
    }
  else if (OEBlurtEvent *prev = prev_gest . Find (id))
    { gestures . Find (id) . ZeroTime ();
      if ((int64) (e -> Progress ()) == prev -> RepeatOrdinal ())
        return OB_OK;
      pe = new OEBlurtRepeatEvent ();
    }
  else
    return OB_OK;
  (~pe) -> SetUtterance ("leap-circle");
  (~pe) -> SetProvenance (e -> Provenance ());
  (~pe) -> SetRepeatOrdinal ((int64) e -> Progress ());
  PortageEvent (~pe, atm);
  prev_gest . Put (id, ~pe);

  return OB_OK;
}

ObRetort LeapToOEBlurtGlimpser::OELeapSwipeAppear (OELeapSwipeAppearEvent *e,
                                                   Atmosphere *atm)
{ int64 id = e -> id;
  ObRef <OEBlurtEvent *> pe = NULL;
  if (! gestures . KeyIsPresent (id))
    { pe = new OEBlurtAppearEvent ();
      FatherTime t;
      gestures . Put (id, t);
    }
  else if (prev_gest . Find (id))
    { gestures . Find (id) . ZeroTime ();
      pe = new OEBlurtRepeatEvent ();
    }
  else
    return OB_OK;
  (~pe) -> SetUtterance ("leap-swipe");
  (~pe) -> SetProvenance (e -> Provenance ());
  PortageEvent (~pe, atm);

  return OB_OK;
}

ObRetort LeapToOEBlurtGlimpser::OELeapScreenTapAppear (
  OELeapScreenTapAppearEvent *e, Atmosphere *atm)
{ ObRef <OEBlurtEvent *> pe = new OEBlurtAppearEvent ();
 (~pe) -> SetUtterance ("leap-screen-tap");
 (~pe) -> SetProvenance (e -> Provenance ());
 PortageEvent (~pe, atm);

 return OB_OK;
}

ObRetort LeapToOEBlurtGlimpser::OELeapKeyTapAppear (OELeapKeyTapAppearEvent *e,
                                                    Atmosphere *atm)
{ ObRef <OEBlurtEvent *> pe = new OEBlurtAppearEvent ();
 (~pe) -> SetUtterance ("leap-key-tap");
 (~pe) -> SetProvenance (e -> Provenance ());
 PortageEvent (~pe, atm);

 return OB_OK;
}


// ObRetort LeapToOEPntgGlimpser::OELeapHandMove (OELeapHandMoveEvent *e,
//                                                  Atmosphere *atm)
// { Str prov = e -> Provenance ();
//   ObRef <OEPointingMoveEvent *> pe = new OEPointingMoveEvent ();
//   (~pe) -> SetProvenance (prov);
//   (~pe) -> SetPhysOrigAndThrough (e -> PhysOrigin (), e -> PhysThrough ());
//   (~pe) -> SetPrevOrigAndThrough (e -> PhysOrigin (), e -> PhysThrough ());
//   (~pe) -> SetWordstamp ("leap-pointer");
//   if (pointers . KeyIsPresent (prov))
//     pointers . Find (prov) . ZeroTime ();
//   else
//     { FatherTime t;
//       pointers . Put (prov, t);
//       ObRef <OEPointingAppearEvent *> pea = new OEPointingAppearEvent ();
//       (~pea) -> SetProvenance (prov);
//       (~pe) -> SetPhysOrigAndThrough (e -> PhysOrigin (), e -> PhysThrough ());
//       (~pe) -> SetPrevOrigAndThrough (e -> PhysOrigin (), e -> PhysThrough ());
//       (~pea) -> SetWordstamp ("leap-pointer");
//       PortageEvent (~pea, NULL);
//     }
//   PortageEvent (~pe, NULL);
//   return OB_OK;
// }

// void LeapGlimpser::LeapCircle (LMCircle const &c, LMFrame const &f)
// { if (c . 
//   ObRef <OEBlurtAppearEvent *> be = new OEBlurtAppearEvent ();
//   (~be) -> SetProvenance (c . prov);
//   (~be) -> SetUtterance ("leap-circle");
//   PortageEvent (~be, NULL);
// }
// 
// void LeapGlimpser::LeapCircle (const &c, LMFrame const &f)
// { ObRef <OEBlurtAppearEvent *> be = new OEBlurtAppearEvent ();
//   (~be) -> SetProvenance (c . prov);
//   (~be) -> SetUtterance ("leap-circle");
//   PortageEvent (~be, NULL);
// }
// 
// void LeapGlimpser::LeapCircle const &c, LMFrame const &f)
// { ObRef <OEBlurtAppearEvent *> be = new OEBlurtAppearEvent ();
//   (~be) -> SetProvenance (c . prov);
//   (~be) -> SetUtterance ("leap-circle");
//   PortageEvent (~be, NULL);
// }
// 
// void LeapGlimpser::LeapCircle const &c, LMFrame const &f)
// { ObRef <OEBlurtAppearEvent *> be = new OEBlurtAppearEvent ();
//   (~be) -> SetProvenance (c . prov);
//   (~be) -> SetUtterance ("leap-circle");
//   PortageEvent (~be, NULL);
// }
