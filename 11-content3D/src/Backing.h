/* (c)  Oblong Industries */


#ifndef BACKING_H
#define BACKING_H

#include <libBasement/TWrangler.h>
#include <libNoodoo/ShowyThing.h>
#include <libAfferent/RoddedGlimpser.h>
#include <libImpetus/OEPointing.h>


namespace oblong {

namespace sample {

class Backing  :  public noodoo::ShowyThing,
                  public impetus::OEPointing::Evts
{ PATELLA_SUBCLASS (Backing, noodoo::ShowyThing);
OB_PROTECTED:
  loam::Str who_is_flying;
  basement::TWrangler *translation;
  bool has_translation;
  loam::Vect last_translation;
  loam::Vect accum_translation;
  loam::Vect harden_origin;

  bool CanFly (impetus::OEPointingEvent *ev) const;
public:
  Backing ();

  //  *NEW*
  virtual loam::ObRetort PreDraw (noodoo::VisiFeld *feld,
                                  basement::Atmosphere *atm);

  //  *NEW*
  virtual loam::ObRetort PostDraw (noodoo::VisiFeld *feld,
                                   basement::Atmosphere *atm);

  virtual loam::ObRetort Travail (basement::Atmosphere *atm);

  virtual loam::ObRetort OEPointingHarden (impetus::OEPointingHardenEvent *e,
            basement::Atmosphere *atm);
  virtual loam::ObRetort OEPointingSoften (impetus::OEPointingSoftenEvent *e,
            basement::Atmosphere *atm);
  virtual loam::ObRetort OEPointingMove   (impetus::OEPointingMoveEvent   *e,
            basement::Atmosphere *atm);

  loam::ObRetort MetabolizeCamReset (const plasma::Protein &prt,
                                     basement::Atmosphere *atm);

  //  some static helper stuff
  static loam::Str pool_name;
  static afferent::RoddedGlimpser *wanding;
  static afferent::RoddedGlimpser *gripe_pointing;
  static void SetupPointing (basement::SpaceFeld *context);
  static void ListenToPointing (basement::KneeObject *obj);
};

}  } // namespaces

#endif
