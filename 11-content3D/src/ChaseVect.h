
/* (c)  oblong industries */

#ifndef CHASE_VECT_NEVER_FORGETS_TO_GO_CHASING_COMMA_SHEL
#define CHASE_VECT_NEVER_FORGETS_TO_GO_CHASING_COMMA_SHEL


#include <libBasement/SoftVect.h>
#include <libBasement/SoftFloat.h>


namespace oblong {  namespace basement {
using namespace oblong::plasma;
using namespace oblong::loam;


class ChaseVect  :  public SoftVect
{ PATELLA_SUBCLASS (ChaseVect, SoftVect);

 OB_PRIVATE:
  ObRef <SoftVect *> current;
  ObRef <SoftVect *> target;
  ObRef <SoftFloat *> fract;
  Vect temp;

 public:
  ChaseVect ()  :  SoftVect (),
         current (new SoftVect),
         target (new SoftVect (Vect (0.0, 0.0, 0.0))),
         fract (new SoftFloat (0.05))
    { }

  ChaseVect (const Vect &c)  :  SoftVect (c),
         current (new SoftVect (c)),
         target (new SoftVect (c)),
         fract (new SoftFloat (0.05))
    { }

  ChaseVect (const Vect &c, const Vect &t, float64 f)  :  SoftVect (c),
         current (new SoftVect (c)),
         target (new SoftVect (t)),
         fract (new SoftFloat (f))
    { }

  ChaseVect (const Vect &c, float64 f)  :  SoftVect (c),
         current (new SoftVect (c)),
         target (new SoftVect (c)),
         fract (new SoftFloat (f))
    { }

  ChaseVect (SoftVect *c, SoftVect *t, SoftFloat *f)
           :  SoftVect (c  ?  c -> Val ()  :  Vect (0.0, 0.0, 0.0)),
         current (c ? c : new SoftVect),
         target (t ? t : new SoftVect (Vect (0.0, 0.0, 0.0))),
         fract (f ? f : new SoftFloat (0.05))
    { }

  ChaseVect (const ChaseVect &otra)  :  SoftVect (otra),
         current ((~otra.current) -> Dup ()),
         target ((~otra.target) -> Dup ()),
         fract ((~otra.fract) -> Dup ())
    { }

  virtual ~ChaseVect ()
    { }

  virtual ChaseVect *Dup ()  const
    { return new ChaseVect (*this); }

  ChaseVect &CopyFrom (const ChaseVect &otra)
    { super::CopyFrom (otra);
      current = (~otra.current) -> Dup ();
      target = (~otra.target) -> Dup ();
      fract = (~otra.fract) -> Dup ();
      return *this;
    }

  const Vect &CurrentVal ()  const
    { return (~current) -> Val (); }
  void InstallCurrent (SoftVect *c)
    { ShouldFreshenUp ();   if (c)  current = c; }
  SoftVect *CurrentSoft ()
    { return ~current; }
  const Vect &SetCurrent (const Vect &c)
    { ShouldFreshenUp ();   return (~current) -> Set (c); }
  const Vect &SetCurrentHard (const Vect &c)
    { ShouldFreshenUp ();   return (~current) -> SetHard (c); }

  const Vect &TargetVal ()  const
    { return (~target) -> Val (); }
  void InstallTarget (SoftVect *t)
    { ShouldFreshenUp ();   if (t)  target = t; }
  SoftVect *TargetSoft ()
    { return ~target; }
  const Vect &SetTarget (const Vect &t)
    { ShouldFreshenUp ();   return (~target) -> Set (t); }
  const Vect &SetTargetHard (const Vect &t)
    { ShouldFreshenUp ();   return (~target) -> SetHard (t); }

  float64 FractionVal ()  const
    { return (~fract) -> Val (); }
  void InstallFraction (SoftFloat *f)
    { ShouldFreshenUp ();   if (f)  fract = f; }
  SoftFloat *FractionSoft ()
    { return ~fract; }
  float64 SetFraction (float64 f)
    { ShouldFreshenUp ();   return (~fract) -> Set (f); }
  float64 SetFractionHard (float64 f)
    { ShouldFreshenUp ();   return (~fract) -> SetHard (f); }

  virtual const Vect &Set (const Vect &v)
    { ShouldFreshenUp ();   return (~target) -> Set (v); }

  virtual const Vect &SetGoal (const Vect &v)
    { ShouldFreshenUp ();   return (~target) -> SetGoal (v); }

  virtual const Vect &SetHard (const Vect &v)
    { ShouldFreshenUp ();
      (~target) -> SetHard (v);
      return (~current) -> SetHard (v);
    }

  virtual const Vect &GoalVal ()  const
    { return CurrentVal (); }

  virtual const Vect &Offset (const Vect &delta)
    { (~current) -> Offset (delta);
      return super::Offset (delta);
    }

  virtual ObRetort Inhale (Atmosphere *atm)
    { if (IsAlreadyCurrent (atm))
        return OB_ALREADY_CURRENT;

      MakeCurrent (atm);
      changed_during_this_quantum = false;

      SoftVect *c = ~current;
      SoftVect *t = ~target;
      SoftFloat *f = ~fract;

      Soft_PushAtmo (atm);
      c -> Inhale (atm);
      t -> Inhale (atm);
      f -> Inhale (atm);
      Soft_PopAtmo (atm);

      if (! needs_freshening
          &&  c -> IsStatic ()
          &&  t -> IsStatic ()
          &&  f -> IsStatic ()
          &&  f -> Val ()  ==  0.0)
        return OB_OK;

      temp = c -> GoalVal ()
        +  (t -> Val () - c -> GoalVal ()) * (0.2 / (1.0 + f -> Val ()));
      if (temp . SquaredDistFrom (t -> Val ()) < 0.00001)
        { temp = t -> Val ();
          c -> SetHard (temp);
          val = c -> Val ();
          changed_during_this_quantum = false;
          needs_freshening = false;
          return OB_EXPENDED_EFFORT;
        }

      c -> SetHard (temp);
      val = c -> Val ();

      needs_freshening = false;
      changed_during_this_quantum = true;
      return OB_EXPENDED_EFFORT;
    }
};


} }   // so passes the musky breath of namespace basement and namespace oblong


#endif
