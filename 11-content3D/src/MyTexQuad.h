#ifndef MY_TEX_QUAD_H
#define MY_TEX_QUAD_H

#include <libBasement/SWrangler.h>
#include <libNoodoo/TexQuad.h>
#include <libImpetus/OEPointing.h>

namespace oblong  {

namespace sample {

class MyTexQuad  :  public  noodoo::TexQuad,
                    public impetus::OEPointing::Evts
{ PATELLA_SUBCLASS (MyTexQuad, noodoo::TexQuad);
OB_PRIVATE:
  basement::SWrangler *hover_w;

  loam::Str owner;

public:
  MyTexQuad (basement::ImageClot *ic);

  virtual loam::ObRetort OEPointingMove   (impetus::OEPointingMoveEvent   *e,
            basement::Atmosphere *atm);

};


}  } //  namespaces

#endif
