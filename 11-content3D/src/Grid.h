#ifndef FADED_GRID_H
#define FADED_GRID_H

#include <libNoodoo/ShowyThing.h>

namespace oblong  {

namespace sample  {

class FadedGrid  :  public  noodoo::ShowyThing
{ PATELLA_SUBCLASS (FadedGrid, noodoo::ShowyThing);
OB_PRIVATE:
  float64 size;
  float64 steps;
public:
  FadedGrid (const float64 _size, const float64 _steps);

  virtual void DrawSelf (noodoo::VisiFeld *vf, basement::Atmosphere *atm);

  virtual noodoo::ShowyThing::BSphere BoundingSphere ()  const;

};


}  } //  namespaces

#endif
