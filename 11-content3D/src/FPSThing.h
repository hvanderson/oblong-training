#ifndef FPS_THING_H
#define FPS_THING_H

#include <libLoam/c++/FatherTime.h>
#include <libNoodoo/ShowyThing.h>
#include <libNoodoo/GlyphoString.h>

namespace oblong  {

namespace sample {

class FPSThing  :  public  noodoo::ShowyThing
{ PATELLA_SUBCLASS (FPSThing, noodoo::ShowyThing);
OB_PRIVATE:
  noodoo::GlyphoString *fps_str;
  int64 frames;
  loam::FatherTime timer;
public:
  FPSThing ();

  virtual void DrawSelf (noodoo::VisiFeld *vf, basement::Atmosphere *atm);

  virtual loam::ObRetort PreDraw (noodoo::VisiFeld *feld,
                                  basement::Atmosphere *atm);

  virtual loam::ObRetort PostDraw (noodoo::VisiFeld *feld,
                                   basement::Atmosphere *atm);
};


}  } //  namespaces

#endif
