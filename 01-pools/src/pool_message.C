
/* (c)  oblong industries */

#include <typeinfo>

#include <libLoam/c++/ArgParse.h>
#include <libBasement/LogLogger.h>
#include <libBasement/UrDrome.h>

using namespace oblong::loam;
using namespace oblong::basement;


static Str message_pool = "my-pool";

class MyDrome  :  public  UrDrome
{ PATELLA_SUBCLASS (MyDrome, UrDrome);
public:
  MyDrome (const Str &drome_name, const ObTrove <Str> &cmdline_leftovers)  :
    super (drome_name, cmdline_leftovers)
    { //  setup pool participation
      UrDrome::OutermostDrome () -> PoolParticipate (message_pool, this);

      //  listen to pool deposits with descrip 'message'
      AppendMetabolizer (Slaw::List ("message"),
                         &MyDrome::MetabolizeMessage2);
                         //&MyDrome::MetabolizeMessage);


      //  BEGIN CODE BLOCK
      //  - listen to pool deposits  with descrip 'echo'


      //  END CODE BLOCK
    }

  ObRetort MetabolizeMessage2 (const Protein &prt,
                              Atmosphere *atm)
  { //  decode protein and check for valid ingest
    const Slaw& s = prt . Ingests();
    int msg;
    OB_LOG_INFO ("new message, HEY= %i", (int)s.Count());

    if (s . Find ("int") . Into (msg)){
        OB_LOG_INFO ("TYPE: %i", ( msg )); 

        PoolDeposit (message_pool, Protein (
              Slaw::List ("message"), Slaw::Map ("int",( msg - 1 ))));
      }
    return OB_OK;
  }

  ObRetort MetabolizeMessage (const Protein &prt,
                              Atmosphere *atm)
  { //  decode protein and check for valid ingest
    const Slaw& s = prt . Ingests();
    Str msg;
    if (s . Find ("text") . Into (msg))
      { OB_LOG_INFO ("new message, text = %s", msg . utf8 ());

        //  deposit an acknowledgement
        PoolDeposit (message_pool, Protein (
              Slaw::List ("acknowledge"), Slaw::Map ("text", msg)));
      }

    return OB_OK;
  }

  //  BEGIN CODE BLOCK
  //  - write a metabolizer for 'echo'
  //  - while the ingest 'repeat' is greater zero,
  //    deposit another 'echo' protein in the same pool,
  //    decrease the value of 'repeat' with every deposit



  // END CODE BLOCK

};


int main (int ac, char **av)
{ // parse command line arguments
  ArgParse ap (ac, av, "[protein files...]");
  ap . ArgString ("message-pool", "\aname of message pool", &message_pool);
  ap . EasyFinish (0, ArgParse::UNLIMITED);

  OB_LOG_INFO ("using message pool: %s", message_pool. utf8 ());

  const Str dromename = "pool-message";
  MyDrome *instance = new MyDrome (dromename, ap . Leftovers ());
  instance -> SetAlwaysReadyToRespire (true);
  instance -> SetLogger (new LogLogger);
  instance -> Respire ();
  instance -> Delete ();

  return (EXIT_SUCCESS);
}

