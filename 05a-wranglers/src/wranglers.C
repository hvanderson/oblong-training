
/* (c)  oblong industries */


#include <libLoam/c++/ArgParse.h>
#include <libBasement/LogLogger.h>
#include <libNoodoo/VFBunch.h>
#include <libNoodoo/VisiDrome.h>
#include <libNoodoo/TexQuad.h>
#include <libMedia/MediaRegistry.h>
#include <libBasement/SineFloat.h>
#include <libBasement/SineColor.h>
#include <libBasement/SineVect.h>
#include <libBasement/LinearColor.h>
#include <libBasement/AsympVect.h>
#include <libBasement/QuadraticVect.h>
#include <libBasement/LinearVect.h>
// *NEW*
#include <libBasement/RWrangler.h>
#include <libBasement/TWrangler.h>
#include <libBasement/SWrangler.h>
#include <libBasement/QWrangler.h>


using namespace oblong::loam;
using namespace oblong::basement;
using namespace oblong::noodoo;

static void AppendToAllVisiFelds (ShowyThing *thing);


TexQuad* CreateTexQuad (const Vect &there, const Str &image_file, VisiFeld *vf)
{ ImageClot *image = ImageClot::FindOrLoadBySourceName (image_file);

  const float64 base_width = vf -> Width () * 0.1;
  const float64 base_height = vf -> Height () * 0.1;

  TexQuad *tex = new TexQuad (image);
  tex -> SetRetainAspectImplicitly (true);
  tex -> SetSizeHard (image -> Width (), image -> Height ());
  tex -> InscribeInside (base_width, base_height);
  tex -> OrientLikeFeld (vf);
  tex -> SetLoc (there);
  AppendToAllVisiFelds (tex);
  return tex;
}

ObRetort Setup (VFBunch* bunch, Atmosphere* atm)
{ //  get first visifeld
  VisiFeld *vf = VisiFeld::NthOfAllVisiFelds (0);

  const int N = VisiFeld::NumAllVisiFelds ();
  for (int i = 0  ;  i < N  ;  ++i)
    { VisiFeld *_vf = VisiFeld::NthOfAllVisiFelds (i);
      _vf -> SetBackgroundColor (ObColor (0.22, 0.22, 0.24, 1.0));
    }

  const Vect tex_loc = vf -> LocOf (SpaceFeld::V::Center, SpaceFeld::H::Center);
  TexQuad *tex1 = CreateTexQuad (tex_loc, "box_01.png", vf);
  OB_LOG_INFO ("tex = %s", tex1 -> Name () . utf8 ());


  //  BEGIN CODE BLOCK
  //  - make tex1 twice its original size using an SWrangler

  //  END CODE BLOCK


  //  BEGIN CODE BLOCK
  //  - create a new TexQuad from 'box_02.png'
  //  - translate it 100 units along the over direction and
  //    50 units along the up direction of the main VisiFeld
  //    using a TWrangler


  //  END CODE BLOCK



  //  BEGIN CODE BLOCK
  //  - create a new TexQuad from 'box_03.png'
  //  - rotate 45 degrees counter clock-wise around norm direction


  //  END CODE BLOCK

  //  BEGIN CODE BLOCK
  //  - create a new TexQuad from 'box_04.png'
  //  - combine all previous transformations such that
  //  - it's twice its original size
  //  - rotated 45 degrees around its center
  //  - translates 100 units along over and 50 units along up


  //  END CODE BLOCK


  return OB_OK;
}

static void AppendToAllVisiFelds (ShowyThing *thing)
{ const int N = VisiFeld::NumAllVisiFelds ();
  for (int i = 0  ;  i < N  ;  ++i)
    { VisiFeld *vf = VisiFeld::NthOfAllVisiFelds (i);
      vf -> AppendChild (thing);
    }
}


int main (int ac, char **av)
{ Str dummy;
  // parse command line arguments
  ArgParse ap (ac, av, "[protein files...]");
  ap . ArgString ("sample-param", "\asample param", &dummy);
  ap . EasyFinish (0, ArgParse::UNLIMITED);

  OB_LOG_INFO ("dummy param: %s", dummy . utf8 ());

  const Str dromename = "wranglers";
  VisiDrome *instance = new VisiDrome (dromename, ap . Leftovers ());
  instance -> SetAlwaysReadyToRespire (true);
  instance -> SetLogger (new LogLogger);
  instance -> FindVFBunch () -> AppendPostFeldInfoHook (Setup, "post_hook");
  instance -> Respire ();
  instance -> Delete ();

  return (EXIT_SUCCESS);
}

