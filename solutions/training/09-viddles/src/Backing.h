/* (c)  Oblong Industries */


#ifndef BACKING_H
#define BACKING_H

#include <libNoodoo/VidQuad.h>
#include <libNoodoo/FlatThing.h>
#include <projects/quartermaster/ViddleSet.h>

namespace oblong {

namespace sample {

class Backing  :  public noodoo::FlatThing
{ PATELLA_SUBCLASS (Backing, noodoo::FlatThing);
OB_PROTECTED:
  loam::ObMap <loam::Str, noodoo::VidQuad*,
               loam::NoMemMgmt, loam::WeakRef> vids_by_name;

public:
  Backing ();

  void AddViddle (quartermaster::Viddle *v);
  void RemoveViddle (quartermaster::Viddle *v);

  static loam::ObRetort ViddleLive (quartermaster::ViddleSet *vs,
                                    quartermaster::Viddle *v,
                                    basement::Atmosphere *atm,
                                    Backing *);

  static loam::ObRetort ViddleDied (quartermaster::ViddleSet *vs,
                                    quartermaster::Viddle *v,
                                    basement::Atmosphere *atm,
                                    Backing*);

};

}  } // namespaces

#endif
