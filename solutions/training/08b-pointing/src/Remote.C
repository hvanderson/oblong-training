
/* (c)  Oblong Industries */


#include <libLoam/c/ob-rand.h>
#include "Remote.h"

Str Remote::full_remote_pool_name;
Str Remote::full_remote_data_pool_name;

Vect WrangledLoc (LocusThing *l)
{ if (l -> NthCumuMats (0))
    { Matrix44 pMat = l -> NthCumuMats (0) -> point_mat;
      return pMat . TransformVect (l -> Loc ());
    }
  else return Vect (.0, .0, .0);
}

RWrangler *MakeRotation (ShowyThing *what, Vect center, Vect axis, double angle)
{ RWrangler *rw = new RWrangler (center, axis, angle);
  what -> AppendWrangler (rw);
  return rw;
}

Remote::Tracker::Tracker (Remote *rr)
    :  LocusThing (),
    touch (0),
    ratchet_state (-1),
    angle (0.0),
    normal_displacement (0.0),
    lost (false),
    vanished (false)
{ provenance = "none";
  model = "none";
  os = "none";
  version = "none";
  name = "none";
  unique_identifier = "none";
  interaction_mode = "none";
  life_timer = new FatherTime ();
  life_timer -> SetTime (-1.0);
  life_timer -> SetSecondsPerSecond (-1.0);
  //  let's start with the cursor in the center of the reference feld
  angles . Zero ();
  prev_angles . Zero ();
  remote = rr;

  SetLocHard (- remote->rsf -> Norm ());
  aim = - remote->rsf -> Norm ();
  prev_aim = - remote->rsf -> Norm ();

  last_in_feld_aim = aim;
  last_in_feld_angles = angles;

  VisiFeld::NthOfAllVisiFelds (0) -> AppendChild (this);
  vertical = MakeRotation (this, Vect (.0, .0, .0), remote->rsf -> Over (), .0);
  horizontal = MakeRotation (this, Vect (.0, .0, .0), remote->rsf -> Up (), .0);
}

ObRetort Remote::Tracker::Travail (Atmosphere *atm)
{ if (provenance == "none")
    return OB_OK;
  double t = life_timer -> CurTime ();
  if (t >= 0.0)
    { vertical -> SetAngleHard (angles.y);
      horizontal -> SetAngleHard (- angles.x);
      //  actually, aim will be one frame behind  : (
      aim = WrangledLoc (this);
    }
  if (t < MAX_LIFE - RECOVERY_LIFE && t > MAX_LIFE - VANISH_LIFE && lost)
    { DBG ("recovering Tracker");
      lost = false;
      aim = prev_aim = last_in_feld_aim;
      angles = last_in_feld_angles;
      prev_angles . Zero ();
      vertical -> SetAngleHard (angles.y);
      horizontal -> SetAngleHard (- angles.x);
      remote -> BroadcastPointingMoveEvent
              (provenance, aim, prev_aim, .0, atm);
    }
  if (t < MAX_LIFE - VANISH_LIFE && t > 0.0 && ! vanished)
    { DBG ("vanishing Tracker");
      remote -> BroadcastPointingVanishEvent
              (provenance, aim, atm);
      vanished = true;
    }
  if (t < MAX_LIFE && t > MAX_LIFE - VANISH_LIFE && t > 0.0 && ! vanished)
    { // controversial
      Str temp = Str (provenance);
      temp . Downcase ();
      if (! strstr (temp . utf8 (), "wii"))  // only if it is not a wiimote
        { remote -> BroadcastPointingMoveEvent
               (provenance, aim, /*prev_*/aim, .0, atm);
        }
    }
  if (t < .0 /*&& provenance != "none"*/)
    { WARN ("deactivating Tracker" + name);
      provenance = "none";  //  free to reassign
      model = "deactivated";
      os = "deactivated";
      version = "deactivated";
      name = "deactivated";
      unique_identifier = "deactivated";
      interaction_mode = "deactivated";
      life_timer -> SetTime (-1.0);
      //  reset aim
      angles . Zero ();
      prev_angles . Zero ();
      aim = - remote->rsf -> Norm ();
      last_in_feld_aim = aim;
      last_in_feld_angles = angles;
      lost = false;
      prev_aim = - remote->rsf -> Norm ();
      vertical -> SetAngleHard (angles.y);
      horizontal -> SetAngleHard (- angles.x);
    }
  return OB_OK;
}

Remote* Remote::instance = NULL;

Remote* Remote::Instance (const Str &hostname_with_pools, SpaceFeld* referenceSpaceFeld)
{ if (instance == NULL)
    instance = new Remote (hostname_with_pools, referenceSpaceFeld);
  return instance;
}

ImageClot *Remote::ExtractImageFromProtein (const Protein &prt)
{ Slaw ing = prt . Ingests ();
  int width;
  int height;
  Str image_format;
  ing . MapFind ("image-format") . Into (image_format);
  ing . MapFind ("image-width") . Into (width);
  ing . MapFind ("image-height") . Into (height);
  DBG (image_format);
  ImageClot *ic = NULL;
  if (image_format == "image/jpeg")
    { DBG ("jpeg");
      JpegImageClot *jic = (JpegImageClot *)JpegImageClot::NewEmpty (width, height, 3);
      jic -> ReadJpegFromSlaw (ing . MapFind ("image-data") . SlawValue (), "jpeg");
      ic = (ImageClot *)jic;
    }
  if (image_format == "RAW_RGBA" || image_format == "image/x-rgba")
    { DBG ("raw");
      ic = ImageClot::NewEmpty (width, height, 3);
      const unsigned char *image_data;
      ing . MapFind ("image-data") . Into (image_data);
      unsigned char *addr = (unsigned char *) ic -> ImageData ();
      int j, k;
      for (int i = 0  ;  i < ing . MapFind ("image-data") . Count () / 4  ;  i++)
        { j = i * 4;
          k = i * 3;
          addr[k + 2] = image_data[j + 0];
          addr[k + 1] = image_data[j + 1];
          addr[k + 0] = image_data[j + 2];
        }
      ic -> FlipVertically ();  //  flip it
    }
  return ic;
}

Remote::Remote (const Str &hostname_with_pools, SpaceFeld *referenceSpaceFeld)  :  NestyThing ()
{ DBG ("creating Remote");

  WARN ("********************************************************");
  WARN ("*                                                      *");
  WARN ("*  It is mandatory to run                              *");
  WARN ("*    pool-server-zeroconf-adapter with argument:       *");
  WARN ("*        -t remote               (on Linux)            *");
  WARN ("*    or                                                *");
  WARN ("*        -t _remote               (on Mac OS X)        *");
  WARN ("*  (otherwise the service will not be found)           *");
  WARN ("********************************************************");

  if (hostname_with_pools == "")
    { full_remote_pool_name = REMOTE_POOL_NAME;
      full_remote_data_pool_name = REMOTE_DATA_POOL_NAME;
    }
  else
    { full_remote_pool_name = "tcp://" + hostname_with_pools + "/" + REMOTE_POOL_NAME;
      full_remote_data_pool_name = "tcp://" + hostname_with_pools + "/" + REMOTE_DATA_POOL_NAME;
    }
  WARN ("Using remote pool: " + full_remote_pool_name);
  WARN ("Using remote data pool: " + full_remote_data_pool_name);

  sensitivity = 1.0;
  internal_glimpser = new Glimpser ();
  if (referenceSpaceFeld)
    rsf = referenceSpaceFeld;
  else
    rsf = VisiFeld::NthOfAllVisiFelds (0);

  origin = rsf -> PhysLoc () + rsf -> PhysDist () * rsf -> PhysNorm ();

  DBG ("Remote: created remote pool");
  ObRetort pret = Pool::Create (full_remote_pool_name, Pool::MMAP_SMALL, false);
  if (! pret . IsSplend ())
    WARN ("Remote: could not create pool, maybe it is already there...");
  else
    DBG ("Remote: remote pool created");
  ObRetort pret2 = Pool::Create (full_remote_data_pool_name, Pool::MMAP_MEDIUM, false);
  if (! pret2 . IsSplend ())
    WARN ("Remote: could not create remote data pool, maybe it is already there...");
  else
    DBG ("Remote: created remote data pool");
  UrDrome::OutermostDrome () -> PoolParticipate (full_remote_pool_name, this);
  UrDrome::OutermostDrome () -> PoolParticipate (full_remote_data_pool_name, this);
  AppendMetabolizer (Slaw::List ("remote-pointing"),
      &Remote::MetabolizePointing, "remote-pointing");
  AppendMetabolizer (Slaw::List ("remote-hello"),
      &Remote::MetabolizeHello, "remote-hello");
  maxY = .7;
  minY = -.7;
  //  create trackers
  for (int i = 0  ;  i < MAX_TRACKERS  ;  i++)
    AppendChild (new Tracker (this));
}

bool Remote::BroadcastPointingMoveEvent (const Str &provenance, Vect orig,
                           Vect thro, Atmosphere *atm, double angle)
{ OEPointingMoveEvent *e = new OEPointingMoveEvent ();
  e -> SetPhysOrigAndThrough (orig, thro);
  e -> SetPrevOrigAndThrough (orig, thro);

  e -> AppendWhatnot ("angle", Slaw (angle));

  Vect hit_point;
  SpaceFeld *sf = SpaceFeld::ClosestWhackedFeld (e -> PhysOrigin (),
        e -> PhysThrough (), &hit_point);
  e -> SetProvenance (provenance);
  e -> SetGenus ("mobile");
  ObRef <OEPointingMoveEvent *> re (e);
  internal_glimpser -> PortageEvent (~re, atm);
  if (sf)
    return true;
  return false;
}

bool Remote::BroadcastPointingMoveEvent (const Str &provenance, Vect aim,
                           Vect prev, double normal_displacement, Atmosphere *atm, double angle)
{ OEPointingMoveEvent *e = new OEPointingMoveEvent ();
  e -> SetPhysOrigAndThrough (origin + normal_displacement * aim, origin
        + normal_displacement * aim + aim);
  e -> SetPrevOrigAndThrough (origin, origin + prev);

  e -> AppendWhatnot ("angle", Slaw (angle));

  Vect hit_point;
  SpaceFeld *sf = SpaceFeld::ClosestWhackedFeld (e -> PhysOrigin (),
        e -> PhysThrough (), &hit_point);
  e -> SetProvenance (provenance);
  e -> SetGenus ("mobile");
  ObRef <OEPointingMoveEvent *> re (e);
  internal_glimpser -> PortageEvent (~re, atm);
  if (sf)
    return true;
  return false;
}

void Remote::BroadcastDisplacementMoveEvent (const Str &provenance, Vect displ,
      Atmosphere *atm)
{ OEDisplacementMoveEvent *e = new OEDisplacementMoveEvent ();
  e -> SetCurrentLocationAndOrientation (displ, rsf -> Norm (), rsf -> Over ());
  e -> SetProvenance (provenance);
  e -> SetGenus ("mobile");
  ObRef <OEDisplacementMoveEvent *> re (e);
  internal_glimpser -> PortageEvent (~re, atm);
}

Glimpser *Remote::InternalGlimpser ()
{ return internal_glimpser; }

void Remote::BroadcastPointingVanishEvent (const Str &provenance, Vect aim,
      Atmosphere *atm)
{ OEPointingVanishEvent *e = new OEPointingVanishEvent ();
  e -> SetProvenance (provenance);
  e -> SetGenus ("mobile");
  ObRef <OEPointingVanishEvent *> re (e);
  internal_glimpser -> PortageEvent (~re, atm);
}

void Remote::BroadcastPointingAppearEvent (const Str &provenance, Vect aim,
      Atmosphere *atm)
{ OEPointingAppearEvent *e = new OEPointingAppearEvent ();
  e -> SetProvenance (provenance);
  e -> SetGenus ("mobile");
  ObRef <OEPointingAppearEvent *> re (e);
  internal_glimpser -> PortageEvent (~re, atm);
}

void Remote::BroadcastPointingHardenEvent (const Str &provenance, Vect aim,
      Atmosphere *atm)
{ OEPointingHardenEvent *e = new OEPointingHardenEvent ();
  e -> SetPhysOrigAndThrough (origin, origin + aim);
  e -> SetProvenance (provenance);
  e -> SetGenus ("mobile");
  ObRef <OEPointingHardenEvent *> re (e);
  internal_glimpser -> PortageEvent (~re, atm);
}

void Remote::BroadcastPointingSoftenEvent (const Str &provenance, Vect aim,
      Atmosphere *atm)
{ OEPointingSoftenEvent *e = new OEPointingSoftenEvent ();
  e -> SetPhysOrigAndThrough (origin, origin + aim);
  e -> SetProvenance (provenance);
  e -> SetGenus ("mobile");
  ObRef <OEPointingSoftenEvent *> re (e);
  internal_glimpser -> PortageEvent (~re, atm);
}

void Remote::BroadcastRatchetProgressEvent (const Str &provenance,
      float64 angle, Atmosphere *atm)
{ OERatchetProgressEvent *e = new OERatchetProgressEvent ();
  e -> SetSpecies (Str () . Sprintf ("%.3f", angle));  // angle is encoded here
  e -> SetProvenance (provenance);
  e -> SetGenus ("mobile");
  ObRef <OERatchetProgressEvent *> re (e);
  internal_glimpser -> PortageEvent (~re, atm);
}

void Remote::BroadcastRatchetSnickEvent (const Str &provenance, const Str &direction,
      Atmosphere *atm)
{ OERatchetSnickEvent *e = new OERatchetSnickEvent ();
  e -> SetSpecies (direction);
  e -> SetProvenance (provenance);
  e -> SetGenus ("mobile");
  ObRef <OERatchetSnickEvent *> re (e);
  internal_glimpser -> PortageEvent (~re, atm);
}

void Remote::BroadcastBlurtAppearEvent (const Str &provenance, const Str &utterance,
      Atmosphere *atm)
{ DBG (utterance);
  OEBlurtAppearEvent *e = new OEBlurtAppearEvent ();
  e -> SetUtterance (utterance);
  e -> SetProvenance (provenance);
  e -> SetGenus ("mobile");
  ObRef <OEBlurtEvent *> re (e);
  internal_glimpser -> PortageEvent (~re, atm);
}

int nextS (int s)
{ int r = s + 1;
  if (r == 3)
    r = 0;
  return r;
}

int prevS (int s)
{ int r = s - 1;
  if (r == -1)
    r = 2;
  return r;
}

void Remote::SetSensitivity (double s)
{ sensitivity = s;
  if (sensitivity < .1)
    sensitivity = .1;
  if (sensitivity > 10.0)
    sensitivity = 10.0;
}

double Remote::Sensitivity ()
{ return sensitivity; }

void Remote::SetMinY (double angle)
{ minY = angle; }

void Remote::SetMaxY (double angle)
{ maxY = angle; }

ObRetort Remote::MetabolizeHello (const Protein &prt,
      Atmosphere *atm)
{ // WARN ("Remote::MetabolizeHello");
  Slaw ing = prt . Ingests ();
  Str prov;
  Str version;
  Str unique_identifier;
  Str interaction_mode;
  Str name;
  Str model;
  Str os;
  ing . MapFind ("provenance") . Into (prov);
  ing . MapFind ("version") . Into (version);
  ing . MapFind ("model") . Into (model);
  ing . MapFind ("unique-identifier") . Into (unique_identifier);
  ing . MapFind ("name") . Into (name);
  ing . MapFind ("interaction_mode") . Into (interaction_mode);
  ing . MapFind ("os") . Into (os);
  Remote::Tracker *t = FindTracker (prov, atm);
  if (t)
    { t->name = name;
      t->version = version;
      t->model = model;
      t->unique_identifier = unique_identifier;
      t->interaction_mode = interaction_mode;
      t->os = os;
      // WARN ("Setting tracker for provenance " + prov + " name (and the rest) to " + name);
      t->life_timer -> SetTime (MAX_LIFE);
    }
  // else
    // WARN ("Remote: Could not find or assign a new tracker.");
  return OB_OK;
}

ObRetort Remote::AppendEventTarget (KneeObject *ko)
{ return internal_glimpser -> AppendEventTarget (ko); }

ObRetort Remote::RemoveEventTarget (KneeObject *ko)
{ return internal_glimpser -> RemoveEventTarget (ko); }

Remote::Tracker *Remote::FindTracker (const Str &prov,
      Atmosphere *atm)
{ //  find the right tracker
  Tracker *tracker = 0;
  ObCrawl <KneeObject *> cr = this -> ChildCrawl ();
  while ((! cr . isempty ()))
    if (Tracker *t = dynamic_cast <Tracker *>
          (cr . popfore ()))
      if (t->provenance == prov)
        return t;
  //  if there is no owned tracker, find a free one and assign it
  ObCrawl <KneeObject *> cr2 = this -> ChildCrawl ();
  while ((! cr2 . isempty ()))
    if (Tracker *t2 =
          dynamic_cast <Tracker *> (cr2 . popfore ()))
      if (t2->provenance == "none") // if (! added)
        { t2->provenance = prov;
          tracker = t2;
          // added = true;
          DBG ("activating Tracker");
          MoveChildTo (tracker, -1);
          BroadcastPointingAppearEvent
                (tracker->provenance, tracker->aim, atm);
          tracker->vanished = false;
          return tracker;
        }
  return NULL;
}

//  this is called every time a new protein is deposited in the pool
//  (i.e. mobile device sensors' updates in our present case)
ObRetort Remote::MetabolizePointing (const Protein &prt,
      Atmosphere *atm)
{ Slaw ing = prt . Ingests ();
  Str prov = "";
  ing . MapFind ("provenance") . Into (prov);

  if (prov . Length () == 0)
    ing . MapFind ("prov") . Into (prov);

  Tracker *tracker = FindTracker (prov, atm);
  if (! tracker)
    return OB_OK;

  double t = tracker->life_timer -> CurTime ();
  if (t < MAX_LIFE - VANISH_LIFE && t > .0 && tracker->vanished)
    { BroadcastPointingAppearEvent
            (tracker->provenance, tracker->aim, atm);
      tracker->vanished = false;
      DBG ("Tracker reappears");
    }
  tracker->life_timer -> SetTime (MAX_LIFE);
  //  extract buttons' statuses from the protein
  ing . MapFind ("number-of-touches") .
        Into (tracker->number_of_touches);
  ing . MapFind ("touch") . Into (tracker->touch);
  if (tracker->touch != TapAndTouch)
    { if (tracker->prev_tap_and_touch)
      { BroadcastPointingSoftenEvent
              (tracker->provenance, tracker->aim, atm);
      }
    }
  if (tracker->touch != TapAndTouch)
    tracker->prev_tap_and_touch = false;
  int new_ratchet_state;
  ing . MapFind ("ratchet_state") . Into (new_ratchet_state);
  Vect vect;
  if (ing . MapFind ("vect") . Into (vect))
    { if (tracker->touch > NoTouch)
        { int mode = 0;
          ing . MapFind ("mode") . Into (mode);
          if (mode == Absolute)
            { tracker->angles . Zero ();
              tracker->angles.x = vect.x;
              tracker->angles.y = vect.y;
            }
          else
            { //  calculate new 'angles'
              vect.x *= sensitivity;
              vect.y *= sensitivity;
              Vect tmp = tracker->angles;
              tracker->angles = tmp * .6
                    + .4 * (vect + tmp);
            }
          // limits for the vertical angle
          if (tracker->angles.y > maxY)
            tracker->angles.y = maxY;
          if (tracker->angles.y < minY)
            tracker->angles.y = minY;
          // continuous swipe
          double swipe_x = .0;
          double swipe_y = .0;
          ing . MapFind ("swipe_x") . Into (swipe_x);
          ing . MapFind ("swipe_y") . Into (swipe_y);  // "pan"
          if (fabs (swipe_x) > .0 || fabs (swipe_y) > .0)
            { Vect d = Vect (swipe_x, .0, swipe_y);
              tracker->normal_displacement = - swipe_y * 2000.0;
// #define DISPLACEMENT_MOVE 1
#ifdef DISPLACEMENT_MOVE
              BroadcastDisplacementMoveEvent (
                    tracker->provenance, d, atm);
#endif
            }

          tracker->vertical -> SetAngleHard (tracker->angles.y);
          tracker->horizontal -> SetAngleHard (- tracker->angles.x);
          //  actually, aim will be one frame behind  : (
          tracker->aim = WrangledLoc (tracker);
          if (tracker->touch == TapAndTouch)
            if (! tracker->prev_tap_and_touch)
              BroadcastPointingHardenEvent (tracker->provenance,
                    tracker->aim, atm);
            tracker->prev_tap_and_touch =
                  (tracker->touch == TapAndTouch);
        }
      else
        tracker->normal_displacement = .0;  //  no touch

      unsigned char swipe_direction = 0;
      ing . MapFind ("swipe") . Into (swipe_direction);
      if (swipe_direction == 0)
        ing . MapFind ("swipe_direction") . Into (swipe_direction);
      if (swipe_direction > 0)
        printf ("swipe direction %d\n", swipe_direction);
      if (/*tracker->touch != TapAndTouch &&*/ swipe_direction > 0)  //  no swipes if hardening
        BroadcastBlurtAppearEvent (tracker->provenance,
              SwipeUtterances[swipe_direction], atm);


      double whatnot_angle = 0.0;
      ing . MapFind ("angle") . Into (whatnot_angle);
      if (vect.z != 0.0)
        whatnot_angle = vect.z;
      // printf ("vect.z is %f\n", vect.z);
      if (BroadcastPointingMoveEvent (tracker->provenance,
            tracker->aim, tracker->prev_aim,
            tracker->normal_displacement, atm, whatnot_angle))
        { tracker->last_in_feld_aim = tracker->aim;
          tracker->last_in_feld_angles = tracker->prev_angles;
        }
      else
        tracker->lost = true;
      //  update previous cursor position memory
      tracker->prev_angles = tracker->angles;
      tracker->prev_aim = tracker->aim;
    }
  double newAngle;
  ing . MapFind ("angle") . Into (newAngle);
// #define RATCHET_PROGRESS 1
#ifdef RATCHET_PROGRESS
  BroadcastRatchetProgressEvent (tracker->provenance, newAngle, atm);
#endif
#ifdef RATCHET_SNICK
  if (tracker->ratchet_state >= 0)
    { if (new_ratchet_state == prevS (tracker->ratchet_state))
        BroadcastRatchetSnickEvent (tracker->provenance, "neg", atm);
      if (new_ratchet_state == nextS (tracker->ratchet_state))
        BroadcastRatchetSnickEvent (tracker->provenance, "pos", atm);
    }
#endif
  tracker->ratchet_state = new_ratchet_state;
  return OB_OK;
}

Str Remote::RecipientDescrip (const Str &recipient)
{ return "recipient:" + recipient;
}

char *CreateGUID ()
{ char *b = new char[32 + 4 + 1];
  int32 r1 = ob_rand_int32 (INT32_MIN, INT32_MAX);
  int32 r2 = ob_rand_int32 (0, INT16_MAX);
  int32 r3 = ob_rand_int32 (0, INT16_MAX);
  int32 r4 = ob_rand_int32 (0, INT16_MAX);
  int32 r5 = ob_rand_int32 (0, INT16_MAX);
  int32 r6 = ob_rand_int32 (INT32_MIN, INT32_MAX);
  sprintf (b, "%08X-%04X-%04X-%04X-%04X%08X", r1, r2, r3, r4, r5, r6);
  // printf ("%s\n", b);
  return b;
}

Protein Remote::TextfieldRequestProtein (const Str &recipient, const Str &hint, bool vibrate)
{ Str conversation_id = Str (CreateGUID ());
  if (hint != "")
    return Protein (Slaw::List ("remote-request-textfield", REMOTE_PROTEINS_FORMAT_VERSION, RecipientDescrip (recipient)),
          Slaw::Map ("show-hint", true, "hint", hint, "vibrate", vibrate,
          "conversation-id", conversation_id));
  else
    return Protein (Slaw::List ("remote-request-textfield", REMOTE_PROTEINS_FORMAT_VERSION, RecipientDescrip (recipient)),
          Slaw::Map ("show-hint", false, "hint", "you should not see this on device", "vibrate", vibrate,
          "conversation-id", conversation_id));
}

Str Remote::DepositTextfieldRequest (const Protein &p)
{ UrDrome::OutermostDrome () -> PoolDeposit (full_remote_data_pool_name, p);
  Str guid;
  p . Ingests () . MapFind ("conversation-id") . Into (guid);
  return guid;
}

Protein Remote::ImageRequestProtein (const Str &recipient, const Str &hint,
      int max_width, int max_height, bool vibrate)
{ Str conversation_id = Str (CreateGUID ());
  if (hint != "")
    return Protein (Slaw::List ("remote-request-image", REMOTE_PROTEINS_FORMAT_VERSION, RecipientDescrip (recipient)),
          Slaw::Map ("show-hint", true, "hint", hint, "vibrate", vibrate, "max-width", max_width, "max-height", max_height,
          "conversation-id", conversation_id));
  else
    return Protein (Slaw::List ("remote-request-image", REMOTE_PROTEINS_FORMAT_VERSION, RecipientDescrip (recipient)),
          Slaw::Map ("show-hint", false, "hint", "you should not see this on device", "vibrate", vibrate,
          "max-width", max_width, "max-height", max_height, "conversation-id", conversation_id));
}

bool Remote::SupportsAdvancedFunctions (OEPointingEvent *e)
{ Str temp = Str (e->provenance);
  temp . Downcase ();
  if (strstr (temp . utf8 (), "ios"))
    return true;
  if (strstr (temp . utf8 (), "web"))
    return true;
  if (strstr (temp . utf8 (), "android"))
    return true;
  return false;
}

Protein Remote::ShowSkinImageProtein (const Str &recipient, const Str &filename)
{ Str conversation_id = Str (CreateGUID ());
  return Protein (Slaw::List ("remote-show-skin-image", REMOTE_PROTEINS_FORMAT_VERSION, RecipientDescrip (recipient)),
                  Slaw::Map ("skin-name", filename,
                             "conversation-id", conversation_id));
}

void Remote::DepositShowSkinImage (const Protein &p)
{ UrDrome::OutermostDrome () -> PoolDeposit (full_remote_data_pool_name, p);
}

Protein Remote::SaveSkinImageProtein (const Str &recipient, const Str &filename)
{ unsigned char *buffer = new unsigned char [1024 * 1024 * 10];  // 10 megabytes max
  int n = 0;
  FILE *f = fopen (filename, "rb");
  if (f)
    { n = fread (buffer, 1, 1024 * 1024 * 10, f);
    }
  else
    { WARN ("SaveSkinImageProtein cannot open file");
    }
  printf ("read %d bytes\n", n);
  // unsigned char *buffer = new unsigned char [100];
  // memset (buffer, 6, 100);
  return Protein (Slaw::List ("remote-save-skin-image", REMOTE_PROTEINS_FORMAT_VERSION, RecipientDescrip (recipient)),
                Slaw::Map ("skin-name", filename, "skin-format", "image/jpeg", "skin-data", Slaw (buffer, n)));
  delete buffer;
}

void Remote::DepositSaveSkinImage (const Protein &p)
{ UrDrome::OutermostDrome () -> PoolDeposit (full_remote_data_pool_name, p);
}

Protein Remote::ShowSkinColorProtein (const Str &recipient, ObColor c)
{ return Protein (Slaw::List ("remote-show-skin-color", REMOTE_PROTEINS_FORMAT_VERSION, RecipientDescrip (recipient)),
                Slaw::Map ("skin-red", c . R (), "skin-green", c . G (), "skin-blue", c . B ()));
}

Protein Remote::DismissProtein (const Str &recipient, const Str &hint, bool vibrate)
{ Str conversation_id = Str (CreateGUID ());
  if (hint != "")
    return Protein (Slaw::List ("remote-dismiss", REMOTE_PROTEINS_FORMAT_VERSION, RecipientDescrip (recipient)),
                Slaw::Map ("show-hint", true, "hint", hint, "vibrate", vibrate,
                "conversation-id", conversation_id));
  else
    return Protein (Slaw::List ("remote-dismiss", REMOTE_PROTEINS_FORMAT_VERSION, RecipientDescrip (recipient)),
                Slaw::Map ("show-hint", false, "hint", "you should not see this on device", "vibrate", vibrate,
                "conversation-id", conversation_id));
}

void Remote::DepositShowSkinColor (const Protein &p)
{ UrDrome::OutermostDrome () -> PoolDeposit (full_remote_data_pool_name, p);
}

Str Remote::DepositDismiss (const Protein &p)
{ UrDrome::OutermostDrome () -> PoolDeposit (full_remote_data_pool_name, p);
  Str guid;
  p . Ingests () . MapFind ("conversation-id") . Into (guid);
  return guid;
}

Str Remote::DepositImageRequest (const Protein &p)
{ UrDrome::OutermostDrome () -> PoolDeposit (full_remote_data_pool_name, p);
  Str guid;
  p . Ingests () . MapFind ("conversation-id") . Into (guid);
  return guid;
}

Str Remote::ExtractRecipientFromProtein (const Protein &p)
{ Str recipient;
  p . Descrips () . Nth (2) . Into (recipient);
  recipient . Replace (0, 10, "");
  return recipient;
}

Str Remote::ExtractVersionFromProtein (const Protein &p)
{ Str version;
  p . Descrips () . Nth (1) . Into (version);
  version . Replace (0, 8, "");
  return version;
}

ObRetort Remote::Metabolizer::Hello (const Str &provenance, const Str &name,
      const Protein &prt, Atmosphere *atm)
{ // WARN ("Remote::Metabolizer::Hello");
  return OB_OK;
}

ObRetort Remote::Metabolizer::MetabolizeHello (const Protein &prt, Atmosphere *atm)
{ Slaw ing = prt . Ingests ();
  Str provenance;
  Str version;
  Str unique_identifier;
  Str interaction_mode;
  Str name;
  Str model;
  Str os;
  ing . MapFind ("provenance") . Into (provenance);
  if (provenance . Length () == 0)
    ing . MapFind ("prov") . Into (provenance);
  ing . MapFind ("version") . Into (version);
  ing . MapFind ("model") . Into (model);
  ing . MapFind ("unique-identifier") . Into (unique_identifier);
  ing . MapFind ("name") . Into (name);
  ing . MapFind ("interaction-mode") . Into (interaction_mode);
  ing . MapFind ("os") . Into (os);
  /*
  WARN ("Remote::Metabolizer::MetabolizeHello");
  WARN (provenance);
  WARN (version);
  WARN (name);
  WARN (interaction_mode);
  WARN (model);
  WARN (os);
  WARN (unique_identifier);
  */
  return Hello (provenance, name, prt, atm);
}

ObRetort Remote::Metabolizer::TextfieldEdit (const Str &provenance, const Str &text,
      const Str &conversation_id, const Protein &prt, Atmosphere *atm)
{ WARN ("Remote::Metabolizer::TextfieldEdit");
  return OB_OK;
}

ObRetort Remote::Metabolizer::Textfield (const Str &provenance, const Str &text,
      const Str &conversation_id, const Protein &prt, Atmosphere *atm)
{ WARN ("Remote::Metabolizer::Textfield");
  return OB_OK;
}

ObRetort Remote::Metabolizer::MetabolizeTextfieldEdit (const Protein &prt, Atmosphere *atm)
{ Slaw ing = prt . Ingests ();
  Str provenance;
  Str text;
  Str conversation_id;
  ing . MapFind ("conversation-id") . Into (conversation_id);
  ing . MapFind ("text") . Into (text);
  ing . MapFind ("provenance") . Into (provenance);
  WARN ("Remote::Metabolizer::MetabolizeTextfieldEdit");
  WARN (text);
  WARN (provenance);
  return TextfieldEdit (provenance, text, conversation_id, prt, atm);
}

ObRetort Remote::Metabolizer::MetabolizeTextfield (const Protein &prt, Atmosphere *atm)
{ Slaw ing = prt . Ingests ();
  Str provenance;
  Str text;
  Str conversation_id;
  ing . MapFind ("conversation-id") . Into (conversation_id);
  ing . MapFind ("text") . Into (text);
  ing . MapFind ("provenance") . Into (provenance);
  WARN ("Remote::Metabolizer::MetabolizeTextfield");
  WARN (text);
  WARN (provenance);
  return Textfield (provenance, text, conversation_id, prt, atm);
}

ObRetort Remote::Metabolizer::ImageUpload (const Str &provenance,
      const Str &conversation_id, const Protein &prt, Atmosphere *atm)
{ WARN ("Remote::Metabolizer::ImageUpload");
  return OB_OK;
}

ObRetort Remote::Metabolizer::MetabolizeImageUpload (const Protein &prt, Atmosphere *atm)
{ Slaw ing = prt . Ingests ();
  int width;
  int height;
  int orientation;
  Str provenance;
  Str image_format;
  Str conversation_id;
  ing . MapFind ("conversation-id") . Into (conversation_id);
  ing . MapFind ("image-format") . Into (image_format);
  ing . MapFind ("provenance") . Into (provenance);
  ing . MapFind ("image-width") . Into (width);
  ing . MapFind ("image-height") . Into (height);
  ing . MapFind ("image-orientation") . Into (orientation);
  WARN ("Remote::Metabolizer::MetabolizeImageUpload");
  WARN (image_format);
  WARN (provenance);
  printf ("image upload %d %d %d\n", width, height, orientation);
  return ImageUpload (provenance, conversation_id, prt, atm);
}

ObRetort Remote::Metabolizer::ImageUploadAnnouncement (const Str &provenance, int width, int height,
        const Str &conversation_id, const Protein &prt, Atmosphere *atm)
{ WARN ("Remote::Metabolizer::ImageUploadAnnouncement");
  return OB_OK;
}

ObRetort Remote::Metabolizer::MetabolizeImageUploadAnnouncement (const Protein &prt, Atmosphere *atm)
{ Slaw ing = prt . Ingests ();
  int width;
  int height;
  int orientation;
  Str provenance;
  Str image_format;
  Str conversation_id;
  ing . MapFind ("conversation-id") . Into (conversation_id);
  ing . MapFind ("image-format") . Into (image_format);
  ing . MapFind ("provenance") . Into (provenance);
  ing . MapFind ("image-width") . Into (width);
  ing . MapFind ("image-height") . Into (height);
  ing . MapFind ("image-orientation") . Into (orientation);
  WARN ("Remote::Metabolizer::MetabolizeImageUploadAnnouncement");
  WARN (image_format);
  printf ("image upload announcement (provenance: %s) %d %d %d\n",
        provenance . utf8 (), width, height, orientation);
  return ImageUploadAnnouncement (provenance, width, height,
        conversation_id, prt, atm);
}

ObRetort Remote::Metabolizer::Response (const Str &provenance, const Str &response,
        const Str &conversation_id, const Protein &prt, Atmosphere *atm)
{ WARN ("Remote::Metabolizer::Response");
  return OB_OK;
}

ObRetort Remote::Metabolizer::MetabolizeResponse (const Protein &prt, Atmosphere *atm)
{ Str provenance;
  Str version;
  Str response;
  Str conversation_id;
  prt . Ingests () . MapFind ("provenance") . Into (provenance);
  prt . Ingests () . MapFind ("version") . Into (version);
  prt . Ingests () . MapFind ("response") . Into (response);
  prt . Ingests () . MapFind ("conversation-id") . Into (conversation_id);
  WARN ("Remote::Metabolizer::MetabolizeResponse");
  WARN ("remote-response");
  WARN (provenance);
  WARN (version);
  WARN (response);
  WARN ("");
  return Response (provenance, response, conversation_id, prt, atm);
}
