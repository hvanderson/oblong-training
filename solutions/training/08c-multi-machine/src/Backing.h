/* (c)  Oblong Industries */


#ifndef BACKING_H
#define BACKING_H

#include <libNoodoo/TexQuad.h>
#include <libNoodoo/FlatThing.h>
#include <libAfferent/RoddedGlimpser.h>


namespace oblong {

namespace sample {


class Backing  :  public noodoo::FlatThing
{ PATELLA_SUBCLASS (Backing, noodoo::FlatThing);
OB_PROTECTED:
  //  *NEW*
  loam::ObMap <loam::Str, noodoo::TexQuad*,
               loam::NoMemMgmt, loam::WeakRef> images_by_uid;
public:
  Backing ();

  loam::ObRetort MetabolizeNewImage (const plasma::Protein &prt,
                                     basement::Atmosphere *atm);



  //  some static helper stuff
  static loam::Str pool_name;
  static afferent::RoddedGlimpser *wanding;
  static afferent::RoddedGlimpser *gripe_pointing;
  static void SetupPointing (basement::SpaceFeld *context);
  static void ListenToPointing (basement::KneeObject *obj);
};

}  } // namespaces

#endif
